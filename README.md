# ExoTOM - A TOM implementation for observing exoplanet transits


## Project local dev setup
Clone exotom repo
```
git clone https://gitlab.gwdg.de/monet/exotom.git
cd exotom
```

### Create & activate virtual environment for python
```
python3 -m venv exotom_env
source exotom_env/bin/activate
```

### Install tom_iag

Clone tom_iag to top-level of project
```
git clone https://github.com/thusser/tom_iag
cd tom_iag
python setup.py install
cd ..
```

### python dependencies
```
pip install -r requirements.txt
```

### local_settings
For local development create `local_settings.py` at top-level of project with f ollowing content (fill in your observation portal api key) and never commit this!
```
import os

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": os.path.join(BASE_DIR, "db.sqlite3"),
    }
}
FACILITIES = {
    "IAG": {
        "portal_url": "https://observe.monet.uni-goettingen.de",
        "archive_url": "https://archive.monet.uni-goettingen.de",
        "api_key": "<observing_portal_api_key>",
        "archive_api_key": "<archive_api_key>",
    },
    "IAG50Transit": {
        "telescope_class": "0m5",
        "instrument_type": "0M5 SBIG6303E",
        "proposal": "exo",
    },
}
```

### make django migrations
```
# python manage.py makemigrations #(to see migrations)
python manage.py migrate
```

### Create admin user
```
python manage.py createsuperuser
```

### Run dev server
```
python manage.py runserver
```
Open http://127.0.0.1:8000/ in a browser and login.

### Optional: Pre commit hooks

Install pre-commit hook through python package pre-commit
```
pre-commit install
```
It runs a python formatter and clears any output from jupyter notebooks before committing. 
It *aborts* the commit if changs were made, you have to re-add the files.

### Optional: pre-push git hook
Copy the following code to a (.git/hooks/pre-push)[.git/hooks/pre-push]:
```
#!/bin/bash

set -e

docker build . --tag test_exotom
docker rm -f test_exotom
docker run -d --shm-size=1GB --name test_exotom test_exotom
#docker exec -ti test_exotom bash
docker exec test_exotom ./manage.py test
docker rm -f test_exotom
# python manage.py test exotom/test

```
It runs tests within a docker container before pushing. If you want quicker tests, just use
```
docker build . --tag test_exotom
python manage.py test exotom/test
```
which only builds the docker image and runs the tests locally (not in docker container) then.

## Observation Settings
Check the last section of (exotom/settings.py)[exotom/settings.py] for settings of the observing sites, exposure time, and observing window.

## ExoTOM Commands
Run `./manage.py` or `python ./manage.py` to see all available commands.

Calculate transits for next n_days (default 10)
```
./manage.py calculate_transits --n_days 2
```
Calculate and submit all whole-transit observations to IAG50cm and MONET South.
```
./manage.py submit_transit_observations --sites Göttingen Sutherland 
```
Calculate and submit all ingresses/egresses to MONET North.
```
./manage.py submit_transit_observations --sites McDonald --contacts 
```
Update observation status of all targets.
```
./manage.py update_observation_status 
```
Update observation status and run analysis.
```
./manage.py process_new_observations --sites McDonald --contacts 
```
Process specific observation by observing portal request id.
```
./manage.py process_observation_by_id --obs_id 12345 --force_redownload True --method all_ref_stars 
```

## Database interaction, transit analysis
Examples for how to run analysis and interact with target, transit und analysis result data programatically is in (example_notebooks/managing_targets_and_transit_observations.ipynb)[example_notebooks/managing_targets_and_transit_observations.ipynb].

# TODO

- check that at least one ref star and certain minimum number of frames is there
- check if `visible` attribute in TransitObservationDetails is even needed anywhere (as opposed to `observable`)
- put comments in analysis methods
- add lower limit for observation length/number of frames
- whether an analysis is run should be determined by the "COMPLETED" status, because "WINDOW_EXPIRED" can also mean that many images were taken (eg https://observe.monet.uni-goettingen.de/requests/19240/) Instead it should be checked, how many pictures are available.
- create transitanalysisresult object earlier (in current version it is only created in LightCurve class) and pass it through all analysis classes, building up analysis info as the analysis progresses. then logs (like which frames/ref stars are filtered out as outliers etc) can be saved in a string variable in this object.
- naming of data product files (datetime in filename is misleading, it is the date when the data was downloaded, not the date of the transit)
- failed analyses (eg if pipeline did not finish properly) get repeated now and will pile up, some status that makes them "failed" for the analysis would help
- clean up and improve log output, log level etc
- version tagging of analysis results
- extrahiertes/gefittetes modell als light curve mit abspeichern in best_lightcurve_dp abspeichern
- simplify passing back of fit parameters from tess_fit class
