FROM python:3.7-slim
ENV PYTHONUNBUFFERED 1
RUN mkdir /tom
WORKDIR /tom

RUN apt-get -y update && apt-get -y install gcc

ENV VIRTUAL_ENV=/opt/venv
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

RUN /opt/venv/bin/python3 -m pip install --upgrade pip
#RUN /usr/local/bin/python3 -m pip install --upgrade pip

COPY requirements.txt /tom/
RUN pip install numpy==1.20.1
RUN pip install -r requirements.txt
RUN pip install https://github.com/thusser/tom_iag/archive/main.zip

COPY . /tom/
#RUN python manage.py collectstatic --noinput
CMD gunicorn --bind 0.0.0.0:8000 --worker-tmp-dir /dev/shm --workers=2 --threads=4 --worker-class=gthread exotom.wsgi
