import logging

import batman
import numpy as np
import pandas as pd
from astropy.time import Time
from astropy import units as u
from astropy.coordinates import EarthLocation, SkyCoord
from django.db import models
from tom_targets.models import Target
from tom_observations.models import ObservationRecord
from tom_dataproducts.models import DataProduct

from datetime import timedelta

from exotom.settings import (
    OBSERVE_N_SIGMA_AROUND_TRANSIT,
    BASELINE_LENGTH_FOR_WHOLE_TRANSIT,
    BASELINE_LENGTH_FOR_TRANSIT_CONTACT,
)

log = logging.getLogger(__name__)


class Transit(models.Model):
    """A single transit."""

    target = models.ForeignKey(Target, on_delete=models.CASCADE)
    number = models.IntegerField("Transit number")

    start = models.DateTimeField("Time the transit starts")
    mid = models.DateTimeField("Time of mid-transit")
    end = models.DateTimeField("Time the transit ends")

    def mid_uncertainty_in_days(self):
        return (
            (self.target.extra_fields["Epoch (BJD) err"]) ** 2
            + (self.number * self.target.extra_fields["Period (days) err"]) ** 2
        ) ** 0.5

    def edge_uncertainty_in_days(self):
        return (
            (self.target.extra_fields["Epoch (BJD) err"]) ** 2
            + (self.number * self.target.extra_fields["Period (days) err"]) ** 2
            + (self.target.extra_fields["Duration (hours) err"] / 2) ** 2
        ) ** 0.5

    def start_earliest(self, n_sigma: float = 1):
        err = timedelta(days=self.edge_uncertainty_in_days())
        return self.start - err * n_sigma

    def start_latest(self, n_sigma: float = 1):
        err = timedelta(days=self.edge_uncertainty_in_days())
        return self.start + err * n_sigma

    def mid_earliest(self, n_sigma: float = 1):
        err = timedelta(days=self.mid_uncertainty_in_days())
        return self.mid - err * n_sigma

    def mid_latest(self, n_sigma: float = 1):
        err = timedelta(days=self.mid_uncertainty_in_days())
        return self.mid + err * n_sigma

    def end_earliest(self, n_sigma: float = 1):
        err = timedelta(days=self.edge_uncertainty_in_days())
        return self.end - err * n_sigma

    def end_latest(self, n_sigma: float = 1):
        err = timedelta(days=self.edge_uncertainty_in_days())
        return self.end + err * n_sigma

    @property
    def facilities(self):
        return list(
            set(
                [
                    (td.facility, td.site)
                    for td in self.transitobservationdetails_set.all()
                    if td.visible
                ]
            )
        )

    def visible(self, facility):
        return any(
            [
                td.visible
                for td in self.transitobservationdetails_set.filter(facility=facility)
            ]
        )

    def ingress_visible(self, facility):
        return any(
            [
                td.ingress_visible
                for td in self.transitobservationdetails_set.filter(facility=facility)
            ]
        )

    def egress_visible(self, facility):
        return any(
            [
                td.egress_visible
                for td in self.transitobservationdetails_set.filter(facility=facility)
            ]
        )

    def visible_at_site(self, site):
        """Checks if transit is in the sky at the given site."""
        tds = self.transitobservationdetails_set.filter(site=site)
        visible = any([td.visible for td in tds])
        return visible

    def observable_at_site(self, site):
        """Checks if transit is in the sky at the given site and whether star is bright enough and transit deep enough."""
        tds = self.transitobservationdetails_set.filter(site=site)
        observable = any([td.observable for td in tds])
        return observable

    def ingress_visible_at_site(self, site):
        """Checks if transit is in the sky at the given site."""
        tds = self.transitobservationdetails_set.filter(site=site)
        visible = any([td.ingress_visible for td in tds])
        return visible

    def ingress_observable_at_site(self, site):
        """Checks if transit is in the sky at the given site and whether star is bright enough and transit deep enough."""
        tds = self.transitobservationdetails_set.filter(site=site)
        observable = any([td.ingress_observable for td in tds])
        return observable

    def egress_visible_at_site(self, site):
        """Checks if transit is in the sky at the given site."""
        tds = self.transitobservationdetails_set.filter(site=site)
        visible = any([td.egress_visible for td in tds])
        return visible

    def egress_observable_at_site(self, site):
        """Checks if transit is in the sky at the given site and whether star is bright enough and transit deep enough."""
        tds = self.transitobservationdetails_set.filter(site=site)
        observable = any([td.egress_observable for td in tds])
        return observable

    @property
    def details(self):
        return self.transitobservationdetails_set.all()

    @property
    def mag(self):
        return self.target.extra_fields["Mag (TESS)"]

    @property
    def depth(self):
        return self.target.extra_fields["Depth (mmag)"]

    class Meta:
        index_together = [
            ("target", "number"),
        ]

    def __str__(self):
        return f"Target {self.target}, transit number {self.number}, start, mid, end {self.start, self.mid, self.end}"

    def get_observing_window(self) -> (Time, Time):
        n_sigma = OBSERVE_N_SIGMA_AROUND_TRANSIT
        baseline = BASELINE_LENGTH_FOR_WHOLE_TRANSIT * u.min
        start = Time(self.start_earliest(n_sigma=n_sigma)) - baseline
        end = Time(self.end_latest(n_sigma=n_sigma)) + baseline
        return start, end

    def get_ingress_observing_window(self) -> (Time, Time):
        n_sigma = OBSERVE_N_SIGMA_AROUND_TRANSIT
        baseline = BASELINE_LENGTH_FOR_TRANSIT_CONTACT * u.min
        start = Time(self.start_earliest(n_sigma=n_sigma)) - baseline
        end = Time(self.start_latest(n_sigma=n_sigma)) + baseline
        return start, end

    def get_egress_observing_window(self) -> (Time, Time):
        n_sigma = OBSERVE_N_SIGMA_AROUND_TRANSIT
        baseline = BASELINE_LENGTH_FOR_TRANSIT_CONTACT * u.min
        start = Time(self.end_earliest(n_sigma=n_sigma)) - baseline
        end = Time(self.end_latest(n_sigma=n_sigma)) + baseline
        return start, end

    def get_observing_margin_in_mins(self) -> float:
        transit_time_error_in_mins = self.mid_uncertainty_in_days() * 24 * 60
        margin = (
            transit_time_error_in_mins * OBSERVE_N_SIGMA_AROUND_TRANSIT
            + BASELINE_LENGTH_FOR_WHOLE_TRANSIT
        )
        return margin


class TransitObservationDetails(models.Model):
    """Transit information at a given site."""

    transit = models.ForeignKey(Transit, on_delete=models.CASCADE)
    facility = models.TextField("Name of facility.")
    site = models.TextField("Name of site.")

    target_alt_start = models.FloatField("Elevation of target at start of transit")
    target_alt_mid = models.FloatField("Elevation of target at start of transit")
    target_alt_end = models.FloatField("Elevation of target at start of transit")
    sun_alt_mid = models.FloatField("Elevation of sun at mid-transit.")
    moon_alt_mid = models.FloatField("Elevation of moon at mid-transit.")
    moon_dist_mid = models.FloatField("Distance to moon at mid-transit.")

    visible = models.BooleanField("Whether transit is visible", null=True)
    observable = models.BooleanField(
        "Whether transit can be observed at this site", null=True
    )

    ingress_visible = models.BooleanField(
        "Whether transit ingress is visible", null=True
    )
    ingress_observable = models.BooleanField(
        "Whether transit ingress can be observed at this site", null=True
    )

    egress_visible = models.BooleanField("Whether transit egress is visible", null=True)
    egress_observable = models.BooleanField(
        "Whether transit egress can be observed at this site", null=True
    )

    class Meta:
        index_together = [
            ("transit", "facility", "site"),
        ]


class TransitAnalysisResult(models.Model):
    """Collects all information from an analysis of a transit observation."""

    transit = models.ForeignKey(Transit, on_delete=models.RESTRICT)
    observation_record = models.ForeignKey(ObservationRecord, on_delete=models.RESTRICT)

    lightcurve_optimization_method = models.TextField(
        "Method used to find find the best transit light curve"
    )

    all_lightcurves_dataproduct = models.ForeignKey(
        DataProduct,
        related_name="all_lightcurves_dp",
        on_delete=models.CASCADE,
        null=True,
    )
    reference_star_catalog_dataproduct = models.ForeignKey(
        DataProduct,
        related_name="reference_star_catalog_dp",
        on_delete=models.CASCADE,
        null=True,
    )
    best_lightcurve_dataproduct = models.ForeignKey(
        DataProduct, related_name="best_lightcurve_dp", on_delete=models.CASCADE
    )
    fit_report_dataproduct = models.ForeignKey(
        DataProduct, related_name="fit_report_dp", on_delete=models.CASCADE
    )
    fit_image_dataproduct = models.ForeignKey(
        DataProduct, related_name="fit_image_dp", on_delete=models.CASCADE
    )

    best_reference_star_ids = models.JSONField(
        "IDs of reference stars selecting during light curve optimization", default=list
    )
    best_loss_sliding_window_std = models.FloatField(
        "Sliding window standard deviation of optimized relative light curve"
    )
    best_loss_fwhm_correlation_p = models.FloatField(
        "Pearson p of correlation of optimized relative light curve with detrended FWHM"
    )

    fit_chi_squared = models.FloatField("Mean Squared residual from model fit")

    batman_t0 = models.FloatField(
        "Batman transit model t0 (time of inferior conjunction) (JD)"
    )
    batman_per = models.FloatField("Batman transit model per (period) (days)")
    batman_rp = models.FloatField(
        "Batman transit model rp (planet radius) (stellar radii)"
    )
    batman_a = models.FloatField(
        "Batman transit model parameter a (semi-major axis) (stellar radii)"
    )
    batman_inc = models.FloatField(
        "Batman transit model parameter inc (inclination) (degrees)"
    )
    batman_ecc = models.FloatField("Batman transit model parameter ecc (eccentricity)")
    batman_w = models.FloatField(
        "Batman transit model w (longitude of periastron) (degrees)"
    )
    batman_fp = models.FloatField(
        "Batman transit model fp (planet-to-star flux ratio) (for secondary transits)",
        null=True,
    )
    batman_limb_dark = models.TextField("Batman transit model limb darkening model")
    batman_limb_dark_u1 = models.FloatField(
        "Batman transit model limb darkening 1st parameter", null=True
    )
    batman_limb_dark_u2 = models.FloatField(
        "Batman transit model limb darkening 2nd parameter", null=True
    )
    batman_limb_dark_u3 = models.FloatField(
        "Batman transit model limb darkening 3rd parameter", null=True
    )
    batman_limb_dark_u4 = models.FloatField(
        "Batman transit model limb darkening 4th parameter", null=True
    )

    batman_t0_error = models.FloatField(
        "Batman transit model t0 (time of inferior conjunction) (JD) error"
    )
    batman_per_error = models.FloatField(
        "Batman transit model per (period) (days) error", null=True
    )
    batman_rp_error = models.FloatField(
        "Batman transit model rp (planet radius) (stellar radii) error"
    )
    batman_a_error = models.FloatField(
        "Batman transit model parameter a (semi-major axis) (stellar radii) error"
    )
    batman_inc_error = models.FloatField(
        "Batman transit model parameter inc (inclination) (degrees) error"
    )
    batman_ecc_error = models.FloatField(
        "Batman transit model parameter ecc (eccentricity) error"
    )
    batman_w_error = models.FloatField(
        "Batman transit model w (longitude of periastron) (degrees) error"
    )
    batman_fp_error = models.FloatField(
        "Batman transit model fp (planet-to-star flux ratio) (for secondary transits) error",
        null=True,
    )
    batman_limb_dark_u1_error = models.FloatField(
        "Batman transit model limb darkening 1st parameter error", null=True
    )
    batman_limb_dark_u2_error = models.FloatField(
        "Batman transit model limb darkening 2nd parameter error", null=True
    )
    batman_limb_dark_u3_error = models.FloatField(
        "Batman transit model limb darkening 3rd parameter error", null=True
    )
    batman_limb_dark_u4_error = models.FloatField(
        "Batman transit model limb darkening 4th parameter error", null=True
    )

    earth_location_lat = models.FloatField("Latitude of earth location of observation")
    earth_location_lon = models.FloatField("Longitude of earth location of observation")
    earth_location_height = models.FloatField(
        "Elevation of earth location of observation"
    )

    m_airmass = models.FloatField(
        "Airmass detrending parameter: m of transit * m * airmass(t) + b"
    )
    b_airmass = models.FloatField(
        "Airmass detrending parameter: b of transit * m * airmass(t) + b"
    )
    m_airmass_error = models.FloatField(
        "Airmass detrending parameter: m of transit * m * airmass(t) + b error"
    )
    b_airmass_error = models.FloatField(
        "Airmass detrending parameter: b of transit * m * airmass(t) + b error"
    )

    def set_batman_parameters(self, params):
        self.batman_t0 = params.t0
        self.batman_per = params.per
        self.batman_rp = params.rp
        self.batman_a = params.a
        self.batman_inc = params.inc
        self.batman_ecc = params.ecc
        self.batman_w = params.w
        self.batman_fp = params.fp
        self.batman_limb_dark = params.limb_dark
        if params.limb_dark == "uniform":
            self.batman_limb_dark_u1 = None
            self.batman_limb_dark_u2 = None
            self.batman_limb_dark_u3 = None
            self.batman_limb_dark_u4 = None
        elif params.limb_dark == "linear":
            self.batman_limb_dark_u1 = params.u[0]
            self.batman_limb_dark_u2 = None
            self.batman_limb_dark_u3 = None
            self.batman_limb_dark_u4 = None
        elif params.limb_dark == "quadratic":
            self.batman_limb_dark_u1 = params.u[0]
            self.batman_limb_dark_u2 = params.u[1]
            self.batman_limb_dark_u3 = None
            self.batman_limb_dark_u4 = None
        else:
            raise NotImplementedError(
                "Batman params creation for this limb darkening model has not been implemented yet"
            )

    def set_fit_errors(self, fit_errors):
        for key, val in fit_errors.items():
            setattr(self, f"{key}_error", val)

    def get_batman_params(self):
        params = batman.TransitParams()
        params.t0 = self.batman_t0
        params.per = self.batman_per
        params.rp = self.batman_rp
        params.a = self.batman_a
        params.inc = self.batman_inc
        params.ecc = self.batman_ecc
        params.w = self.batman_w
        params.fp = self.batman_fp
        params.limb_dark = self.batman_limb_dark
        if params.limb_dark == "uniform":
            params.u = []
        elif params.limb_dark == "linear":
            params.u = [self.batman_limb_dark_u1]
        elif params.limb_dark == "quadratic":
            params.u = [self.batman_limb_dark_u1, self.batman_limb_dark_u2]
        else:
            raise NotImplementedError(
                "Batman params creation for this limb darkening model has not been implemented yet"
            )

        return params

    def get_lightcurve_df(self):
        return pd.read_csv(
            self.best_lightcurve_dataproduct.data.path,
            index_col=["time", "star", "aperture"],
        )

    def get_target_relative_lightcurve_times_and_errors(self):
        lightcurve_df = self.get_lightcurve_df()
        light_curve = lightcurve_df.loc[
            (slice(None), "target_rel", slice(None)), "flux"
        ]
        light_curve_errors = lightcurve_df.loc[
            (slice(None), "target_rel", slice(None)), "error"
        ]
        times = np.array(lightcurve_df.index.levels[0])
        return times, light_curve, light_curve_errors

    def get_earth_location(self):
        lat, lon, height = (
            self.earth_location_lat,
            self.earth_location_lon,
            self.earth_location_height,
        )
        earth_location = EarthLocation(
            lat=lat * u.deg, lon=lon * u.deg, height=height * u.m
        )
        return earth_location

    def get_transit_name(self, short=False):
        name = "TOI " if not short else ""
        name += f"{self.transit.target.name[4:]} #{self.transit.number}"
        try:
            obs_record_name = self.observation_record.parameters["name"]
        except:
            return ""

        if "INGRESS" in obs_record_name:
            name += " INGRESS" if not short else "I"
        elif "EGRESS" in obs_record_name:
            name += " EGRESS" if not short else "E"
        else:
            name += " WHOLE" if not short else "W"

        return name
