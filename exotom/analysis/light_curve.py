import json
import pprint, sys, os, tempfile
import traceback
from collections import namedtuple
from contextlib import contextmanager
from io import StringIO
from typing import Optional

import batman
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from astropy.coordinates import EarthLocation, AltAz, SkyCoord
from astropy.time import Time
import astropy.units as u
from scipy import optimize
from scipy.interpolate import interpolate

from django.core.files import File
from django.core.files.base import ContentFile
from tom_targets.models import TargetExtra, Target
from tom_observations.models import ObservationRecord
from tom_dataproducts.models import DataProduct
from exotom.models import Transit, TransitAnalysisResult

from exotom.settings import COORDS_BY_INSTRUMENT


@contextmanager
def write_stdout_to_stringbfuffer() -> StringIO:
    string_buffer_stdout = StringIO()
    old_stdout = sys.stdout

    try:
        sys.stdout = string_buffer_stdout
        yield string_buffer_stdout
    finally:
        sys.stdout = old_stdout


FitResult = namedtuple(
    "FitResult",
    [
        "params",
        "m_airmass",
        "b_airmass",
        "fitted_model",
        "baseline_model",
        "chi_squared",
        "fit_errors",
        "fit_report",
    ],
)

sl = pd.IndexSlice


class LightCurve:
    def __init__(
        self,
        best_light_curve_dp,
        best_loss_sliding_window_std=0,
        best_loss_fwhm_correlation_p=0,
    ):
        self.best_light_curve_dp: DataProduct = best_light_curve_dp
        self.observation_record: ObservationRecord = (
            self.best_light_curve_dp.observation_record
        )
        self.best_loss_sliding_window_std = best_loss_sliding_window_std
        self.best_loss_fwhm_correlation_p = best_loss_fwhm_correlation_p

        self.best_light_curve_df: pd.DataFrame = pd.read_csv(
            self.best_light_curve_dp.data.path,
            index_col=["time", "star", "aperture"],
        )
        self.times = np.array(self.best_light_curve_df.index.levels[0])
        self.flux = np.array(
            self.best_light_curve_df.loc[sl[:, "target_rel", :], "flux"]
        )
        self.error = np.array(
            self.best_light_curve_df.loc[
                sl[
                    :,
                    "target_rel",
                ],
                "error",
            ]
        )

        self.target = self.observation_record.target

        self.earth_location = self.get_earth_location()

        # this try-except is needed because in the beginning "transit_id" was not written to the parameters dict
        try:
            self.transit: Transit = Transit.objects.get(
                target=self.target,
                number=self.observation_record.parameters["transit"],
            )
        except (KeyError):
            try:
                self.transit: Transit = Transit.objects.get(
                    id=self.observation_record.parameters["transit_id"]
                )
            except (KeyError, Transit.DoesNotExist):
                self.transit = None

        self.light_curve_name = self.get_lightcurve_name()
        print(f"Using light curve name {self.light_curve_name}")

        self.fit_report_dataproduct: Optional[DataProduct] = None
        self.fit_image_dataproduct: Optional[DataProduct] = None
        self.transit_analysis_result: Optional[TransitAnalysisResult] = None

    def get_earth_location(self):
        lat, lon, height = self.get_earth_location_coordinates()
        earth_location = EarthLocation(
            lat=lat * u.deg, lon=lon * u.deg, height=height * u.m
        )
        return earth_location

    def get_earth_location_coordinates(self):
        try:
            instrument = self.observation_record.parameters["instrument_type"]
        except KeyError:
            # if instrument_type not in parameters, use goettingen camera
            instrument = "0M5 SBIG6303E"
        lat = COORDS_BY_INSTRUMENT[instrument]["latitude"]
        lon = COORDS_BY_INSTRUMENT[instrument]["longitude"]
        height = COORDS_BY_INSTRUMENT[instrument]["elevation"]
        return lat, lon, height

    def get_lightcurve_name(self):
        return json.loads(self.best_light_curve_dp.extra_data)["light_curve_name"]

    def get_lightcurve_optimization_method(self):
        return json.loads(self.best_light_curve_dp.extra_data)[
            "light_curve_optimization_method"
        ]

    def fit(self):
        self.best_fit_result = self.make_best_fit()
        if self.best_fit_result is not None:
            self.save_fit_report_as_dataproduct_and_txt_file(
                self.best_fit_result.fit_report,
                product_id=self.light_curve_name + "_fit_report",
                extra_data=self.light_curve_name,
                data_product_type="transit_fit_report",
            )
            self.create_best_light_curve_and_fit_image()

        return self.create_transit_analysis_result_object()

    def make_best_fit(self) -> FitResult:
        target_extras_list = (
            list(self.transit.target.targetextra_set.all()) if self.transit else None
        )

        fit_result: Optional[FitResult] = None
        if self.transit is not None:
            transit_fit = TessTransitFit(
                self.best_light_curve_df,
                self.transit,
                target_extras_list,
                self.earth_location,
            )
            fit_result = transit_fit.make_simplest_fit_and_report()
        else:
            print(
                "Transit not given, so not doing fit. Returning None from make_best_fit"
            )

        return fit_result

    def save_fit_report_as_dataproduct_and_txt_file(
        self, fit_report, product_id, extra_data, data_product_type
    ) -> DataProduct:

        dp = DataProduct.objects.create(
            extra_data=self.light_curve_name,
            target=self.target,
            observation_record=self.observation_record,
            data_product_type=data_product_type,
        )
        dfile = ContentFile(fit_report)
        dp.data.save(
            product_id + ".txt",
            dfile,
        )
        dp.save()
        self.fit_report_dataproduct = dp
        return dp

    def create_best_light_curve_and_fit_image(self):
        tmpfile = tempfile.NamedTemporaryFile()

        self.save_plot_of_transit_data_and_fit(tmpfile.name)

        if tmpfile:
            dp = DataProduct.objects.create(
                target=self.best_light_curve_dp.target,
                observation_record=self.best_light_curve_dp.observation_record,
                data_product_type="image_file",
            )
            outfile_name = os.path.basename(self.best_light_curve_dp.data.file.name)
            filename = os.path.splitext(outfile_name)[0] + ".svg"
            with open(tmpfile.name, "rb") as f:
                dp.data.save(filename, File(f), save=True)
                dp.save()
            tmpfile.close()
            self.fit_image_dataproduct = dp

    def save_plot_of_transit_data_and_fit(self, file_name):
        # filter out negative values
        not_negative_mask = np.logical_and(self.flux > 0, self.flux - self.error > 0)
        flux = self.flux[not_negative_mask]
        error = self.error[not_negative_mask]
        times = self.times[not_negative_mask]

        fig, (fit_axes, residuals_axes) = plt.subplots(
            2,
            figsize=(12, 8),
            sharex="all",
            gridspec_kw={
                "height_ratios": [1, 0.3],
                "hspace": 0.06,
                "top": 0.92,
                "bottom": 0.08,
                "left": 0.08,
                "right": 0.68,
            },
        )
        fig.patch.set_facecolor("white")

        baseline_function = self.best_fit_result.baseline_model
        fit_axes.errorbar(
            times,
            flux / baseline_function(times),
            yerr=error / baseline_function(times),
            marker="o",
            linewidth=1,
            markersize=5,
            linestyle="None",
            color=[0, 0, 1, 0.3],
            label="Measured relative\ntarget flux",
        )

        if self.best_fit_result is not None:
            fit_function = self.best_fit_result.fitted_model
            label = self.get_best_fit_params_legend_string()

            times_across_whole_transit = np.linspace(
                min(times[0], Time(self.transit.start_earliest()).jd),
                max(times[-1], Time(self.transit.end_latest()).jd),
                1000,
            )
            fit_axes.plot(
                times_across_whole_transit,
                fit_function(times_across_whole_transit)
                / baseline_function(times_across_whole_transit),
                color="red",
                label=label,
                zorder=10,
            )

            # plot residuals
            residuals_axes.axhline(y=0, color="black")
            residuals_axes.set_ylabel("Residual Flux")
            residuals_axes.set_xlabel("t [BJD]")
            residuals_axes.errorbar(
                times,
                flux / baseline_function(times)
                - fit_function(times) / baseline_function(times),
                yerr=error / baseline_function(times),
                marker="o",
                linewidth=1,
                markersize=5,
                linestyle="None",
                color=[0, 0, 1, 0.3],
            )

        # save current axis limits
        x_min, x_max = fit_axes.get_xlim()
        y_min, y_max = fit_axes.get_ylim()

        ### draw predicted values ###
        predicted_color = "green"

        def plot_line_with_errors(
            value,
            error_value,
            line_label=None,
            axis="x",
            offset=0,
            color=predicted_color,
            linestyle="dashed",
        ):
            # plot a line over entire axes at value, with error bar at edge of axes
            x_width = x_max - x_min
            y_width = y_max - y_min
            if axis == "y":
                xs = [x_min - x_max, x_max - x_width * 0.05, x_max + y_max]
                ys = [np.power(10, -value / 1000 / 2.5)] * 3
                yerr = [0, 1 - np.power(10, -error_value / 1000 / 2.5), 0]
                xerr = None
            elif axis == "x":
                xs = [value] * 3
                ys = [
                    y_min - y_width,
                    y_max - y_width * (0.05 + 0.025 * offset),
                    y_max + y_width,
                ]
                yerr = None
                xerr = [0, error_value, 0]

            fit_axes.errorbar(
                xs,
                ys,
                yerr=yerr,
                xerr=xerr,
                capsize=5,
                color=color,
                linestyle=linestyle,
                label=line_label,
            )

        try:
            depth = self.target.targetextra_set.get(key="Depth (mmag)").float_value
            depth_error = self.target.targetextra_set.get(
                key="Depth (mmag) err"
            ).float_value

            plot_line_with_errors(
                depth,
                depth_error,
                "Predicted depth",
                axis="y",
                color="lightseagreen",
                linestyle="dotted",
            )
        except TargetExtra.DoesNotExist:
            pass

        try:
            # draw predicted ingress, mid, egress with 1 sigma errors
            tr: Transit = self.transit
            start, err = Time(tr.start).jd, tr.edge_uncertainty_in_days()
            plot_line_with_errors(
                start, err, "Predicted start,\nmid, end time", offset=0
            )

            end, err = Time(tr.end).jd, tr.edge_uncertainty_in_days()
            plot_line_with_errors(end, err, offset=1)

            mid, err = Time(tr.mid).jd, tr.mid_uncertainty_in_days()
            plot_line_with_errors(mid, err, offset=2)
        except:
            traceback.print_exc()
            pass

        ### draw labels etc ###
        plot_title_transit_name = (
            f"Transit {self.transit.target.name} #{self.transit.number}"
        )
        try:
            if "INGRESS" in self.observation_record.parameters["name"]:
                plot_title_transit_name += " INGRESS"
            elif "EGRESS" in self.observation_record.parameters["name"]:
                plot_title_transit_name += " EGRESS"
        except:
            pass

        plt.suptitle(
            f"{plot_title_transit_name} (airmass detrended)\nLight curve optimization method: {self.get_lightcurve_optimization_method()}"
        )
        fit_axes.set_ylabel("Relative Flux")
        # add second axis for magnitude
        second_y_axis = fit_axes.secondary_yaxis(
            "right",
            functions=(
                lambda x: -2.5 * np.log10(1 / np.clip(x, 1e-3, 1e10)),
                lambda y: np.power(10, y / 2.5),
            ),
        )
        second_y_axis.set_ylabel("Mag Difference")
        fit_axes.axhline(y=1, color="grey", zorder=-10)

        fit_axes.set_xlim(x_min, x_max)
        fit_axes.set_ylim(y_min, y_max)

        fit_axes.legend(loc="upper left", bbox_to_anchor=(1.13, 1.0), fontsize=10)

        plt.savefig(file_name, format="svg")
        plt.close(fig)

    def get_best_fit_params_legend_string(self):
        label_list = ["Fit batman model\nw/ airmass detrending:"]

        batman_fitted_parameters = [
            "t0",
            "rp",
            "a",
            "inc",
        ]
        for param in batman_fitted_parameters:
            val, err = (
                getattr(self.best_fit_result.params, param),
                self.best_fit_result.fit_errors[f"batman_{param}"],
            )
            label_list.append(f"{param}: {format_pretty_round(val, err)}")

        # limbdark model and values
        label_list.append(f"limb_dark: {self.best_fit_result.params.limb_dark}")
        val, err = (
            self.best_fit_result.params.u[0],
            self.best_fit_result.fit_errors["batman_limb_dark_u1"],
        )
        label_list.append(f"limb_dark_u1: {format_pretty_round(val, err)}")

        not_fitted_parameters_digits = [
            ["per", 3],
            ["ecc", 1],
            ["w", 1],
        ]
        for param, digits in not_fitted_parameters_digits:
            val = getattr(self.best_fit_result.params, param)
            label_list.append(
                f"{param}: {val}" if type(val) == str else f"{param}: {val:.{digits}f}"
            )

        detrend_parameters = [
            "m_airmass",
            "b_airmass",
        ]
        for param in detrend_parameters:
            val, err = (
                getattr(self.best_fit_result, param),
                self.best_fit_result.fit_errors[param],
            )
            label_list.append(f"{param}: {format_pretty_round(val, err)}")

        label = "\n".join(label_list)
        # label_without_last_new_line = label[:-1]
        return label

    def create_transit_analysis_result_object(self):
        result = TransitAnalysisResult()
        result.transit = self.transit
        result.observation_record = self.observation_record
        result.lightcurve_optimization_method = (
            self.get_lightcurve_optimization_method()
        )

        result.reference_star_catalog_dataproduct = (
            DataProduct.objects.filter(
                target=self.target,
                observation_record=self.observation_record,
                data_product_type="reference_star_catalog",
            )
            .order_by("created")
            .last()
        )
        result.all_lightcurves_dataproduct = (
            DataProduct.objects.filter(
                target=self.target,
                observation_record=self.observation_record,
                data_product_type="transit_all_light_curves",
            )
            .order_by("created")
            .last()
        )
        result.best_lightcurve_dataproduct = self.best_light_curve_dp
        result.fit_report_dataproduct = self.fit_report_dataproduct
        result.fit_image_dataproduct = self.fit_image_dataproduct

        result.best_reference_star_ids = [
            star
            for star in self.best_light_curve_df.index.levels[1]
            if star not in ["target", "target_rel"]
        ]
        result.best_loss_sliding_window_std = self.best_loss_sliding_window_std
        result.best_loss_fwhm_correlation_p = self.best_loss_fwhm_correlation_p

        result.fit_chi_squared = self.best_fit_result.chi_squared

        lat, lon, height = self.get_earth_location_coordinates()
        result.earth_location_lat = lat
        result.earth_location_lon = lon
        result.earth_location_height = height

        result.set_batman_parameters(self.best_fit_result.params)
        result.set_fit_errors(self.best_fit_result.fit_errors)

        result.m_airmass = self.best_fit_result.m_airmass
        result.b_airmass = self.best_fit_result.b_airmass
        result.save()

        self.transit_analysis_result = result
        print(f"Created TransitAnalysisResult object with id {result.id}")
        return result


class TessTransitFit:
    def __init__(
        self,
        light_curve_df: pd.DataFrame,
        transit: Transit,
        target_extras: [TargetExtra],
        earth_location: EarthLocation = None,
    ):
        self.light_curve_df = light_curve_df
        self.times = np.array(light_curve_df.index.levels[0])
        self.flux = np.array(light_curve_df.loc[sl[:, "target_rel", :], "flux"])
        self.error = np.array(
            light_curve_df.loc[
                sl[
                    :,
                    "target_rel",
                ],
                "error",
            ]
        )

        self.transit = transit
        self.target_extras: [TargetExtra] = target_extras
        self.earth_location = earth_location

        self.params: batman.TransitParams = self.get_transit_params_object()
        self.m_airmass = -0.5
        self.b_airmass = 1.5

        self.fit_errors = {}

    def make_simplest_fit_and_report(self, light_curve_df: pd.DataFrame = None):
        print("Fitting...")
        with write_stdout_to_stringbfuffer() as string_buffer_stdout:
            try:
                if self.earth_location is not None:
                    # with airmass detrending
                    (
                        params,
                        m_airmass,
                        b_airmass,
                        fitted_model,
                        baseline_model,
                        chi_squared,
                        fit_errors,
                    ) = self.make_simplest_fit_with_airmass_detrending(light_curve_df)

                else:
                    print(
                        "earth_location can't be None. Need earth location to fit (for airmass detrending)."
                    )
            except:
                traceback.print_exc()
                pass

        fit_report = string_buffer_stdout.getvalue()
        print(fit_report)

        return FitResult(
            params,
            m_airmass,
            b_airmass,
            fitted_model,
            baseline_model,
            chi_squared,
            fit_errors,
            fit_report,
        )

    def make_simplest_fit_with_airmass_detrending(
        self, light_curve_df: pd.DataFrame = None
    ):
        if light_curve_df is not None:
            self.light_curve_df = light_curve_df

        ts, ys, errs = self.times, self.flux, self.error
        fit_a_and_t0_func = (
            get_a_t0_and_limb_dark_coeff_fit_function_with_airmass_detrending(
                self.params, ts, self.transit, self.earth_location
            )
        )

        p0 = [
            self.params.a,
            self.params.t0,
            self.params.rp,
            self.params.inc,
            self.params.u[0],
            self.m_airmass,
            self.b_airmass,
        ]

        t0_lower_bound = min(Time(self.transit.start_earliest(n_sigma=2)).jd, ts.min())
        t0_upper_bound = max(Time(self.transit.end_latest(n_sigma=2)).jd, ts.max())
        bounds = [
            [
                0,
                t0_lower_bound,
                0,
                45,
                0,
                -10,
                -10,
            ],
            [
                1e3,  # np.inf,
                t0_upper_bound,
                1,
                90,
                1,
                10,
                10,
            ],
        ]

        print(f"Initial batman TransitParams:")
        pprint.pprint(self.params.__dict__)
        print(
            f"Fit p0 [a, t0, rp, inc, linear_limb_darkening_coeff, m_airmass, b_airmass]:"
        )
        pprint.pprint(p0)
        print(f"Fit bounds:")
        pprint.pprint(bounds)

        print(
            "\nStarting fit of linear function of airmass detrending only (m_airmass, b_airmass)..."
        )

        # first fit only linear parameters of airmass detrending baseline (m_airmass, b_airmass)
        def airmass_only_fit_func(t, m_airmass, b_airmass):
            return fit_a_and_t0_func(t, *p0[:-2], m_airmass, b_airmass)

        popt, pcov = optimize.curve_fit(
            airmass_only_fit_func,
            ts,
            ys,
            sigma=errs,
            absolute_sigma=True,
            p0=p0[-2:],
            bounds=[bounds[0][-2:], bounds[1][-2:]],
            loss="soft_l1",
            method="trf",
            verbose=2,
        )

        print("Airmass popt [m_airmass, b_airmass]\n", popt)
        p0[-2:] = popt

        print("\nStarting full fit with airmass detrending...")
        popt, pcov = optimize.curve_fit(
            fit_a_and_t0_func,
            ts,
            ys,
            sigma=errs,
            absolute_sigma=True,
            p0=p0,
            bounds=bounds,
            x_scale="jac",  # other possibility: give scale for each variable -> ~ [1, .01, .05, .3, .2, .3, .3],
            loss="soft_l1",
            method="trf",
            verbose=2,
            # xtol=None,
        )
        perr = np.sqrt(np.diag(pcov))
        print(
            f"\nFitted parameters and errors: [a, t0, rp, inc, linear_limb_darkening_coeff, m_airmass, b_airmass]:"
            f"\n{popt}\n{perr}"
        )
        print(f"Covariance matrix: \n{pcov}")

        for i, param in enumerate(["a", "t0", "rp", "inc"]):
            setattr(self.params, param, popt[i])
            self.fit_errors[f"batman_{param}"] = perr[i]

        self.fit_errors["batman_ecc"] = 0
        self.fit_errors["batman_w"] = 0

        self.params.u = [popt[4]]
        self.m_airmass = popt[5]
        self.b_airmass = popt[6]
        self.fit_errors["batman_limb_dark_u1"] = perr[4]
        self.fit_errors["m_airmass"] = perr[5]
        self.fit_errors["b_airmass"] = perr[6]

        print(f"\nFinal batman TransitParams:")
        pprint.pprint(self.params.__dict__)

        def fitted_model(times):
            return fit_a_and_t0_func(times, *popt)

        def baseline_model(times):
            airmass_function = get_airmass_function(
                ts, self.transit, self.earth_location
            )
            return self.m_airmass * airmass_function(times) + self.b_airmass

        chi_squared = np.sum((ys - fitted_model(ts)) ** 2 / errs ** 2)

        return (
            self.params,
            self.m_airmass,
            self.b_airmass,
            fitted_model,
            baseline_model,
            chi_squared,
            self.fit_errors,
        )

    def get_ref_star_columns(self, columns):
        ref_star_columns = list(
            filter(
                lambda col: str(col).isdigit() or type(col) == int,
                columns,
            )
        )
        return ref_star_columns

    def get_transit_params_object(self):
        params: batman.TransitParams = batman.TransitParams()
        params.t0 = Time(self.transit.mid).jd
        params.per = self.get_target_extra(key="Period (days)").float_value
        # convert to solar radii
        PLANET_RADIUS_DEFAULT = 2.0
        planet_radius_in_earth_radii = self.get_target_extra(
            key="Planet Radius (R_Earth)"
        ).float_value
        planet_radius_in_solar_radii = (
            planet_radius_in_earth_radii
            if planet_radius_in_earth_radii
            else PLANET_RADIUS_DEFAULT
        ) / 109.2
        DEFAULT_STELLAR_RADIUS = 2.0
        stellar_radius = self.get_target_extra(key="Stellar Radius (R_Sun)").float_value
        stellar_radius_in_solar_radii = (
            stellar_radius if stellar_radius else DEFAULT_STELLAR_RADIUS
        )
        planet_radius_in_stellar_radii = (
            planet_radius_in_solar_radii / stellar_radius_in_solar_radii
        )
        params.rp = planet_radius_in_stellar_radii

        orbit_radius_in_stellar_radii = self.estimate_orbit_radius()
        params.a = orbit_radius_in_stellar_radii

        # estimate safe inclination that guarantees transit but is not on fit bound of 90 degrees
        cos_min_inclination_for_transit = 1 / orbit_radius_in_stellar_radii
        min_inclination_for_transit = np.degrees(
            np.arccos(cos_min_inclination_for_transit)
        )
        params.inc = 90 - (90 - min_inclination_for_transit) / 5

        params.ecc = 0
        params.w = 90
        params.limb_dark = "linear"
        params.u = [0.4]

        return params

    def estimate_orbit_radius(self):
        try:
            # constants
            grav_constant = 6.7e-11
            abs_mag_sun = 4.83
            mass_sun = 2e30
            radius_sun_in_m = 7e8

            # system parameters
            apparent_magnitude = self.get_target_extra(key="Mag (TESS)").float_value
            distance = self.get_target_extra(key="Stellar Distance (pc)").float_value
            period_in_s = (
                self.get_target_extra(key="Period (days)").float_value * 24 * 60 * 60
            )
            DEFAULT_STELLAR_RADIUS = 0.5
            stellar_radius = self.get_target_extra(
                key="Stellar Radius (R_Sun)"
            ).float_value
            radius_star_in_sun_radii = (
                stellar_radius if stellar_radius else DEFAULT_STELLAR_RADIUS
            )

            # calculation
            absolute_magnitude = apparent_magnitude - 5 * (np.log10(distance) - 1)
            L_divided_by_L_sun = np.power(10, 0.4 * (abs_mag_sun - absolute_magnitude))
            # mass-luminosity relation which only holds for main sequence stars!
            mass = mass_sun * np.power(L_divided_by_L_sun, 0.25)
            # kepler 3
            orbit_radius = np.power(
                mass * grav_constant * period_in_s ** 2 / (4 * np.pi), 1 / 3
            )

            # convert to stellar radii
            orbit_radius_in_sun_radii = orbit_radius / radius_sun_in_m
            orbit_radius_in_stellar_radii = (
                orbit_radius_in_sun_radii / radius_star_in_sun_radii
            )
        except:
            orbit_radius_in_stellar_radii = 20

        return orbit_radius_in_stellar_radii

    def get_target_extra(self, key):
        for target_extra in self.target_extras:
            if target_extra.key == key:
                return target_extra


def get_a_t0_and_limb_dark_coeff_fit_function_with_airmass_detrending(
    params, times, transit: Transit, earth_location: EarthLocation
):
    def fit_a_and_t0_func(
        ts, a, t0, rp, inc, linear_limb_darkening_coeff, m_airmass, b_airmass
    ):
        params.a = a
        params.t0 = t0
        params.rp = rp
        params.inc = inc
        params.u = [linear_limb_darkening_coeff]
        model = batman.TransitModel(params, ts)
        airmass_function = get_airmass_function(times, transit, earth_location)
        flux = model.light_curve(params) * (
            m_airmass * airmass_function(ts) + b_airmass
        )
        return flux

    return fit_a_and_t0_func


def get_airmass_function(
    times: np.array, transit: Transit, earth_location: EarthLocation
):
    # extend times so it extrapolates to edges of transit (with errors) is possible
    extended_times = np.linspace(
        min(times[0], Time(transit.start_earliest(n_sigma=1.1)).jd),
        max(times[-1], Time(transit.end_latest(n_sigma=1.1)).jd),
        1000,
    )
    astropy_times = Time(extended_times, format="jd")

    airmass = get_airmass(astropy_times, transit.target, earth_location)
    airmass_func = interpolate.interp1d(extended_times, airmass, kind="linear")
    return airmass_func


def get_airmass(times, target: Target, earth_location: EarthLocation):
    frame = AltAz(obstime=times, location=earth_location)
    target_altaz = SkyCoord(target.ra, target.dec, unit="deg").transform_to(frame)
    airmass = target_altaz.secz
    return airmass


def significant_digits(a):
    eps = 1e-12
    clipped_positive = max(eps, np.abs(a))
    return -int(np.floor(np.log10(clipped_positive)))


def pretty_round(
    val,
    err,
    max_val_digits_after_comma=None,
    min_val_digits=None,
    default_error_digits=2,
):
    val_sig = significant_digits(val)
    err_sig = significant_digits(err) + default_error_digits - 1

    round_to = err_sig
    if max_val_digits_after_comma is not None:
        round_to = min(max_val_digits_after_comma, err_sig)

    min_val_digits_from_comma = val_sig + min_val_digits - 1
    if min_val_digits_from_comma is not None:
        round_to = max(min_val_digits_from_comma, round_to)

    val_rounded = np.around(val, round_to)
    err_rounded = np.around(err, round_to)

    return val_rounded, err_rounded, round_to


def format_pretty_round(
    val, err, max_val_digits_after_comma=5, min_val_digits=2, error_digits=2
):
    val_rounded, err_rounded, digits = pretty_round(
        val, err, max_val_digits_after_comma, min_val_digits, error_digits
    )
    show_decimals = max(digits, 0)
    return f"{val_rounded:.{show_decimals}f}±{err_rounded:.{show_decimals}f}"
