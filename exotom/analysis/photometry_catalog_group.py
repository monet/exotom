import time
import copy
from typing import Union, List, Optional

import numpy as np
from astropy.coordinates import SkyCoord
import astropy.units as u
import matplotlib.pyplot as plt
import pandas as pd

from django.core.files.base import ContentFile
from tom_dataproducts.models import DataProductGroup, DataProduct
from tom_targets.models import Target

MAX_SEPARATION_TO_CATALOG_IN_DEG = 5 / 3600


class TransitPhotometryCatalogGroup:
    def __init__(self, data_product_group: DataProductGroup):
        self.data_product_group = data_product_group

        self.data_products = data_product_group.dataproduct_set.all()
        self.check_data_products_validity()

        self.first_data_product = self.data_products[0]
        self.observation_record = self.first_data_product.observation_record
        self.target: Target = self.first_data_product.target
        self.target_coord = SkyCoord(self.target.ra * u.deg, self.target.dec * u.deg)

        self.transit_name = data_product_group.name
        self.light_curve_name = f"{self.transit_name}_light_curve"

        self.image_catalogs = None

    def check_data_products_validity(self):
        if len(self.data_products) == 0:
            raise ValueError("DataProductGroup contains no dataproducts.")

        for dp in self.data_products:
            try:
                path = dp.data.path
                if path is None or path == "":
                    raise Exception()
            except:
                raise ValueError(
                    f"Dataproduct {repr(dp)} does not have well defined DataProduct.data.path"
                )

        self.check_all_dataproducts_are_photometry_catalogs(self.data_products)

    def check_all_dataproducts_are_photometry_catalogs(self, dps):
        for dp in dps:
            if dp.data_product_type != "image_photometry_catalog":
                raise ValueError(
                    f"DataGroup contains none 'image_photometry_catalog' DataProducts (due to {dp})."
                )

    def load_data(self, verbose=True):
        dps = self.data_products
        print("Starting data load from dataproduct list")
        start = time.time()
        image_catalogs = []
        files = sorted([dp.data.path for dp in dps])
        print(f"Found {len(files)} files.")
        for i, filename in enumerate(sorted(files), 1):
            if verbose and i % 100 == 0:
                print("(%d/%d) Loading %s..." % (i, len(files), filename))

            cat = pd.read_csv(filename)
            image_catalogs.append(cat)
        print("Load took %.2fs" % (time.time() - start,))

        self.image_catalogs = image_catalogs
        return self.image_catalogs

    def save_all_lightcurves_dataproduct_and_file(
        self, all_light_curves_df
    ) -> DataProduct:
        product_id = self.light_curve_name + "_all"
        dp = DataProduct.objects.create(
            product_id=product_id,
            target=self.target,
            observation_record=self.observation_record,
            data_product_type="transit_all_light_curves",
        )
        dfile = ContentFile(all_light_curves_df.to_csv())
        dp.data.save(
            product_id + ".csv",
            dfile,
        )
        dp.save()
        return dp


class PhotometryCatalogMatcher:
    """Matches and extracts all potential ref star and the target light curves from catalogs of transit images."""

    def __init__(
        self,
        image_catalogs: [],
        target_coord: SkyCoord,
        one_image_for_plot: np.ndarray = None,
        flux_column_name_for_matching="fluxaper8",
        max_allowed_pixel_value: float = 8e4,
        max_allowed_source_ellipticity: float = 0.4,
        min_allowed_source_fwhm: float = 2.5,
    ):
        self.flux_column_name_for_matching = flux_column_name_for_matching
        self.full_image_catalogs: [pd.DataFrame] = self.filter_out_incomplete_catalogs(
            image_catalogs
        )
        self.image_catalogs = None
        self.target_coord: SkyCoord = target_coord

        self.one_image_for_plot = one_image_for_plot

        # filters sources by following criteria
        self.max_allowed_pixel_value = max_allowed_pixel_value
        self.max_allowed_source_ellipticity = max_allowed_source_ellipticity
        self.min_allowed_source_fwhm = min_allowed_source_fwhm

        self.ref_catalog: Union[pd.DataFrame, None] = None
        self.ref_star_ids = None
        self.target_id: Union[int, None] = None
        self.matched_image_catalogs_with_target: Union[list, None] = None

        self.light_curve_df = None
        self.fluxrad50_df = None

    def get_target_and_ref_stars_light_curves_df(
        self,
        use_only_n_brightest_ref_sources: int = None,
    ) -> pd.DataFrame:

        self.build_and_match_ref_source_catalog(use_only_n_brightest_ref_sources)
        self.light_curve_df = self.build_lightcurve_df()
        self.fluxrad50_df = self.build_fluxrad50_df()
        return self.light_curve_df

    def build_and_match_ref_source_catalog(
        self,
        use_only_n_brightest_ref_sources,
        force_rebuild=False,
    ):
        flux_column_name: str = self.flux_column_name_for_matching
        if (
            self.ref_catalog is not None
            and self.target_id is not None
            and self.matched_image_catalogs_with_target is not None
        ) and not force_rebuild:
            # ref source catalog already built
            print("Not matching, since matched catalog was found.")
            return

        filtered_image_catalogs = self.filter_out_outlier_frames_with_few_sources(
            self.full_image_catalogs
        )

        ref_catalog, ref_catalog_coords = self.get_ref_catalog_from_catalog(
            filtered_image_catalogs[0]
        )
        self.target_id = self.find_target_id(ref_catalog_coords)
        print(f"Starting with {len(ref_catalog)} reference sources.")

        matched_image_catalogs_with_target = self.match_image_catalogs_to_ref_catalog(
            filtered_image_catalogs, ref_catalog_coords
        )
        matched_image_catalogs_with_target = (
            self.remove_frames_where_target_was_not_matched(
                matched_image_catalogs_with_target, self.target_id
            )
        )
        ref_catalog = self.keep_only_n_brightest_ref_sources_and_target(
            ref_catalog,
            use_only_n_brightest=use_only_n_brightest_ref_sources,
        )
        print("Enforcing that each ref source is exactly once in each frame...")

        self.plot_ref_sources_on_image(ref_catalog)
        (
            ref_catalog,
            matched_image_catalogs_with_target,
        ) = self.enforce_each_ref_source_exactly_once_per_image_and_valid_flux(
            matched_image_catalogs_with_target, ref_catalog, flux_column_name
        )
        self.plot_ref_sources_on_image(ref_catalog)

        ref_catalog = ref_catalog.drop(columns="Unnamed: 0")
        id_column = ref_catalog.pop("id")
        ref_catalog.insert(0, "id", id_column)

        self.ref_catalog = ref_catalog
        self.ref_star_ids = list(self.ref_catalog["id"])
        self.ref_star_ids.remove(self.target_id)

        self.matched_image_catalogs_with_target = matched_image_catalogs_with_target

    def filter_out_outlier_frames_with_few_sources(
        self,
        full_image_catalogs,
        max_remove_fraction=0.2,
        n_sigma=4,
        min_fraction_of_median_ref_source_number=0.8,
    ) -> [pd.DataFrame]:
        number_of_ref_stars = np.array([len(df.index) for df in full_image_catalogs])
        full_image_catalogs_copy = copy.deepcopy(full_image_catalogs)

        # calculate filter threshold on number of ref stars per frame
        median = np.median(number_of_ref_stars)
        std = np.std(number_of_ref_stars)
        threshold = max(
            median - n_sigma * std, min_fraction_of_median_ref_source_number * median
        )

        print(median, std, threshold)
        # select frames with few ref stars
        few_ref_stars_mask = number_of_ref_stars < threshold
        print(np.sum(few_ref_stars_mask))

        # enforce limit on frame removal
        if np.sum(few_ref_stars_mask) > max_remove_fraction * len(full_image_catalogs):
            sorted_indices = np.argsort(number_of_ref_stars)
            remove_indices = sorted_indices[
                : int(max_remove_fraction * len(full_image_catalogs))
            ]
        else:
            remove_indices = np.where(few_ref_stars_mask)[0]

        remove_indices = sorted(list(remove_indices), reverse=True)
        print(
            f"Removing following {len(remove_indices)} frames, because they have to few ref stars: {remove_indices}"
        )
        for i in remove_indices:
            if i < len(full_image_catalogs_copy):
                full_image_catalogs_copy.pop(i)
        return full_image_catalogs_copy

    def get_ref_catalog_from_catalog(self, catalog: pd.DataFrame):
        ref_catalog: pd.DataFrame = copy.deepcopy(catalog)

        ref_catalog = ref_catalog[ref_catalog["peak"] < self.max_allowed_pixel_value]
        ref_catalog = ref_catalog[
            ref_catalog["ellipticity"] < self.max_allowed_source_ellipticity
        ]
        ref_catalog = ref_catalog[ref_catalog["fwhm"] > self.min_allowed_source_fwhm]
        # ref_catalog = ref_catalog[ref_catalog["flux"] > 8e2]
        ref_catalog.index = list(range(len(ref_catalog)))
        ref_catalog["id"] = list(range(len(ref_catalog)))

        ref_catalog_coords = SkyCoord(
            ref_catalog["ra"] * u.deg, ref_catalog["dec"] * u.deg, frame="icrs"
        )
        return ref_catalog, ref_catalog_coords

    def find_target_id(self, ref_catalog_coords):
        target_id, sep, _ = self.target_coord.match_to_catalog_sky(ref_catalog_coords)
        target_id = int(target_id)  # to convert from array to int
        if sep.degree > MAX_SEPARATION_TO_CATALOG_IN_DEG:
            raise Exception(
                f"Best source match for photometry target is too far away ({sep.degree}°)."
            )
        return target_id

    def match_image_catalogs_to_ref_catalog(self, image_catalogs, ref_catalog_coords):
        """add reference source ids to image_catalog in place"""
        for i_cat, cat in enumerate(image_catalogs):
            if i_cat % 100 == 0:
                print("Matching frame %d/%d ..." % (i_cat, len(image_catalogs) - 1))

            cat_coords = SkyCoord(cat["ra"] * u.deg, cat["dec"] * u.deg, frame="icrs")

            ids, separation_angles, _ = cat_coords.match_to_catalog_sky(
                ref_catalog_coords
            )
            ids[separation_angles.degree > MAX_SEPARATION_TO_CATALOG_IN_DEG] = -1
            cat["id"] = ids.astype(int)

        return image_catalogs

    def remove_frames_where_target_was_not_matched(self, image_catalogs, target_id):
        remove_ids = []
        for i_cat, cat in enumerate(image_catalogs):
            counts = cat["id"].value_counts()
            if (not target_id in counts.index) or counts[target_id] != 1:
                remove_ids.append(i_cat)
        print(
            f"Removing {len(remove_ids)} frames because the target star was not matched exactly once."
        )
        for remove_id in reversed(remove_ids):
            image_catalogs.pop(remove_id)

        return image_catalogs

    def keep_only_n_brightest_ref_sources_and_target(
        self, ref_catalog, use_only_n_brightest: int = None
    ):
        if use_only_n_brightest:
            ref_catalog = ref_catalog.sort_values(by=["flux"], ascending=[False])
            filtered_ref_catalog = ref_catalog.iloc[
                : min(use_only_n_brightest, len(ref_catalog.index))
            ]
            if self.target_id not in filtered_ref_catalog.id:
                filtered_ref_catalog.append(
                    ref_catalog[ref_catalog.id == self.target_id]
                )
            return filtered_ref_catalog
        return ref_catalog

    def enforce_each_ref_source_exactly_once_per_image_and_valid_flux(
        self, image_catalogs: [pd.DataFrame], ref_catalog, flux_column_name
    ):
        """Enforce that all ref_sources have been identified *exactly once* in each frame.
        if a ref_source is not *exactly once* in a frame or the flux_column_value is nan, remove that source.
        """
        times = [cat.iloc[0]["time"] for cat in image_catalogs]
        ids = ref_catalog.id
        source_flux_values = pd.DataFrame(index=times, columns=ids)

        for i_cat, catalog in enumerate(image_catalogs):
            if i_cat % 100 == 0:
                print("Checking frame %d/%d ..." % (i_cat, len(image_catalogs) - 1))

            time = catalog.iloc[0].time
            for id in ids:
                try:
                    flux_value_series = catalog[catalog.id == id][flux_column_name]
                    if len(flux_value_series.index) != 1:
                        flux_value = np.nan
                    else:
                        flux_value = flux_value_series.iloc[0]
                except Exception as e:
                    flux_value = np.nan

                source_flux_values.loc[time, id] = flux_value

        any_nan_per_ref_source_series = source_flux_values.isna().any(axis="index")
        remove_ref_sources_ids = any_nan_per_ref_source_series[
            any_nan_per_ref_source_series == True
        ].index

        print(
            f"Removing these ref_sources b/c they are not exactly once in each image or have '{flux_column_name}' value nan: {remove_ref_sources_ids}"
        )
        ref_catalog.drop(labels=remove_ref_sources_ids, axis="index", inplace=True)
        print("Removal done.")
        return ref_catalog, image_catalogs

    def build_lightcurve_df(self):
        print("Building DataFrame of all source light curves...")
        times = [cat.iloc[0]["time"] for cat in self.matched_image_catalogs_with_target]
        stars = ["target"]
        stars.extend(self.ref_star_ids)
        apertures = [
            aper
            for aper in range(1, 15)
            if f"fluxaper{aper}" in self.matched_image_catalogs_with_target[0].columns
            if f"fluxerr{aper}" in self.matched_image_catalogs_with_target[0].columns
        ]
        multi_index = pd.MultiIndex.from_product(
            [times, stars, apertures], names=["time", "star", "aperture"]
        )

        df = pd.DataFrame(index=multi_index, columns=["flux", "error"])
        for aper in apertures:
            flux_name = f"fluxaper{aper}"
            fluxerr_name = f"fluxerr{aper}"

            # for target
            target_light_curve_and_errors = (
                self.extract_time_series_of_columns_for_star_ids(
                    self.matched_image_catalogs_with_target,
                    [self.target_id],
                    [flux_name, fluxerr_name],
                )
            )
            df.loc[
                (slice(None), "target", aper), ["flux", "error"]
            ] = target_light_curve_and_errors

            # for ref stars
            ref_star_light_curve_and_errors = (
                self.extract_time_series_of_columns_for_star_ids(
                    self.matched_image_catalogs_with_target,
                    self.ref_star_ids,
                    [flux_name, fluxerr_name],
                )
            )
            df.loc[
                (slice(None), self.ref_star_ids, aper), ["flux", "error"]
            ] = ref_star_light_curve_and_errors

        df = df.drop_duplicates()
        return df

    def build_fluxrad50_df(self):
        print("Building DataFrame of all fluxrad50 values...")
        times = [cat.iloc[0]["time"] for cat in self.matched_image_catalogs_with_target]
        stars = ["target"] + self.ref_star_ids
        multi_index = pd.MultiIndex.from_product([times, stars], names=["time", "star"])

        col_name = "fluxrad50"
        df = pd.DataFrame(index=multi_index, columns=[col_name])

        # for target
        target_fluxrad50 = self.extract_time_series_of_columns_for_star_ids(
            self.matched_image_catalogs_with_target, [self.target_id], [col_name]
        )
        df.loc[(slice(None), "target"), "fluxrad50"] = target_fluxrad50

        # for ref stars
        ref_star_fluxrad50 = self.extract_time_series_of_columns_for_star_ids(
            self.matched_image_catalogs_with_target, self.ref_star_ids, [col_name]
        )
        df.loc[(slice(None), self.ref_star_ids), col_name] = ref_star_fluxrad50

        df = df.drop_duplicates()
        return df

    def extract_time_series_of_columns_for_star_ids(
        self, catalogs, ids_to_extract: list, column_names: list
    ) -> Optional[List]:
        try:
            time_series = np.concatenate(
                [cat[cat["id"].isin(ids_to_extract)][column_names] for cat in catalogs]
            )  # .reshape((len(catalogs) * len(ids_to_extract), len(column_names)))
        except KeyError as e:
            print(f"Key not found in time series extraction: {e}")
            return None
        return time_series

    def plot_ref_sources_on_image(self, ref_catalog):
        if self.one_image_for_plot is None:
            return
        image = self.one_image_for_plot
        mean = np.mean(image)
        std = np.std(image)
        plt.figure()
        plt.imshow(image, vmin=mean - std, vmax=mean + std)
        for ref in ref_catalog.itertuples():
            # print(ref.x, ref.y)
            plt.errorbar(
                ref.x,
                ref.y,
                xerr=ref.fwhm / 2,
                yerr=ref.fwhm / 2,
                marker="x",
                markersize=10,
                linewidth=3,
                color="black",
            )
        plt.show()

    def filter_out_incomplete_catalogs(self, image_catalogs):

        required_columns = ["ra", "dec", "time", "flux"]
        filtered_catalogs = []
        for icat, cat in enumerate(image_catalogs):
            for required_column in required_columns:
                if required_column not in cat.columns:
                    print(
                        f"Filtering out frame {icat}/{len(image_catalogs)} because column {required_column} is missing."
                    )
                    break
            else:  # nobreak
                filtered_catalogs.append(cat)

        return filtered_catalogs
