import traceback

from tom_observations.models import ObservationRecord

from exotom.analysis.light_curve import LightCurve
from exotom.analysis.transit_observation import TransitObservation
from exotom.analysis.all_light_curves import AllLightCurves
from exotom.management.commands.update_observation_status import (
    update_observation_status_command,
)


def process_new_observations_command():
    update_observation_status_command()

    for observation_record in ObservationRecord.objects.all():
        try:
            transit_observation = TransitObservation(observation_record)
            if (
                transit_observation.observation_record_completed_and_all_lightcurve_dataproduct_not_created()
            ):
                analyze_observation_record(observation_record)

        except Exception as e:
            print(f"Transit analysis failed because of '{e}'. Traceback:")
            traceback.print_exc()


def analyze_observation_record(
    observation_record: ObservationRecord,
    force_redownload=False,
    use_every_nth_timestep=1,
    method="",
):
    try:
        transit_observation = TransitObservation(
            observation_record, use_every_nth_timestep=use_every_nth_timestep
        )
        all_lightcurves_dp = (
            transit_observation.attempt_create_all_lightcurves_dataproduct(
                force_redownload=force_redownload
            )
        )

        all_lcs = AllLightCurves(all_lightcurves_dp, transit_observation.fluxrad50_dp)
        best_lc_dp = all_lcs.create_best_lightcurve(method=method)

        lc = LightCurve(
            best_lc_dp,
            best_loss_sliding_window_std=all_lcs.best_loss_sliding_window_std,
            best_loss_fwhm_correlation_p=all_lcs.best_loss_fwhm_correlation_p,
        )
        lc.fit()

        return lc.transit_analysis_result
    except Exception as e:
        print(
            f"Analysis of ObservationRecord {observation_record} failed due to '{e}'. Traceback:"
        )
        traceback.print_exc()


def analyze_observation_record_by_observation_id(
    observation_id: int, force_redownload=False, use_every_nth_timestep=1, method=""
):
    try:
        obs_record = ObservationRecord.objects.get(observation_id=observation_id)
    except ObservationRecord.DoesNotExist:
        print(f"Couldn't find ObservationRecord with observation_id={observation_id}")
        return

    return analyze_observation_record(
        obs_record,
        force_redownload=force_redownload,
        use_every_nth_timestep=use_every_nth_timestep,
        method=method,
    )
