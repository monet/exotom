import io
import os
import time
import traceback
from datetime import datetime

import requests
import pandas as pd
from astropy.time import Time
from astropy.coordinates import SkyCoord, EarthLocation
import astropy.units as u
from django.core.files.base import ContentFile
from tom_dataproducts.models import DataProduct, DataProductGroup

from exotom.analysis.photometry_catalog_group import PhotometryCatalogMatcher
from exotom.analysis.photometry_catalog_group import TransitPhotometryCatalogGroup
from tom_iag.iag import IAGFacility

from exotom.settings import COORDS_BY_INSTRUMENT


class TransitObservation:
    """Downloads photometry catalogs of transit observations and creates transit_all_lightcurves DataProduct"""

    CALIBRATED_REDUCTION_LEVEL_INT = 1

    def __init__(self, observation_record, use_every_nth_timestep=None):
        self.observation_record = observation_record
        self.use_every_nth_timestep = use_every_nth_timestep
        self.photometry_catalog_dataproduct_group = None

        now_string = datetime.utcnow().replace(microsecond=0).isoformat()
        self.identifier = f"{self.get_transit_name()}_{now_string}"

        self.all_light_curves_df = None
        self.all_light_curves_dp = None
        self.fluxrad50_df = None
        self.fluxrad50_dp = None

    def observation_record_completed_and_all_lightcurve_dataproduct_not_created(self):
        if (
            self.observation_completed()
            and not self.transit_all_lightcurves_dataproduct_exists()
        ):
            return True
        return False

    def observation_completed(self):
        return self.observation_record.status == "COMPLETED"

    def transit_all_lightcurves_dataproduct_exists(self):
        dp = self.attempt_get_most_recent_all_lightcurves_dataproduct()
        if dp is not None:
            return True
        return False

    def attempt_get_most_recent_all_lightcurves_dataproduct(self):
        dps_for_obs_record = DataProduct.objects.filter(
            observation_record=self.observation_record,
            data_product_type="transit_all_light_curves",
        ).order_by("-created")
        if len(dps_for_obs_record) > 0:
            return dps_for_obs_record[0]
        return None

    def attempt_get_most_recent_fluxrad50_dataproduct(self):
        dps_for_obs_record = DataProduct.objects.filter(
            observation_record=self.observation_record,
            data_product_type="fluxrad50",
        ).order_by("-created")
        if len(dps_for_obs_record) > 0:
            return dps_for_obs_record[0]
        return None

    def attempt_create_all_lightcurves_dataproduct(
        self, force_redownload=False, clean_up=True
    ):

        try:
            if not self.observation_completed():
                print(
                    f"Observation {self.observation_record} not COMPLETED (status is {self.observation_record.status})."
                )
                return

            if not force_redownload:
                self.all_light_curves_dp = (
                    self.attempt_get_most_recent_all_lightcurves_dataproduct()
                )
                self.fluxrad50_dp = self.attempt_get_most_recent_fluxrad50_dataproduct()
                if (
                    self.all_light_curves_dp is not None
                    and self.fluxrad50_dp is not None
                ):
                    print(
                        f"Found already existing transit_all_light_curves dataproduct (most recent one):"
                        f" {self.all_light_curves_dp}"
                        f" Using this one. (and most recent fluxrad50 dp)."
                        f" (Pass `force_redownload=True to attempt_create_all_lightcurves_dataproduct to override.)"
                    )
                    return self.all_light_curves_dp

            self.lock_observation_record_for_analysis()
            self.make_photometry_catalog_data_product_group()
            self.create_all_lightcurves_and_fluxrad50_dataproduct()
            return self.all_light_curves_dp
        except Exception as e:
            print(
                f"Analysis of ObservationRecord {self.observation_record} failed due to '{e}'. Traceback:"
            )
            traceback.print_exc()
        finally:
            if clean_up:
                print("Cleaning up downloaded raw photometry catalogs...")
                self.clean_up_image_photometry_catalog_dataproduct_group()
            self.unlock_observation_record_from_analysis()

    def make_photometry_catalog_data_product_group(self):
        self.create_photometry_catalog_dataproduct_group()

        reduced_products = self.get_reduced_data_products_and_check_pipeline_finished()
        if self.use_every_nth_timestep:
            reduced_products = reduced_products[:: self.use_every_nth_timestep]

        for i_product, product in enumerate(reduced_products):
            if i_product % 100 == 0:
                print(
                    f"creating data product {i_product}/{len(reduced_products)}: {product}"
                )

            dp = self.create_image_photometry_catalog_dataproduct_and_add_to_group(
                product
            )
            self.download_and_save_dataproduct_file(dp, product)

    def create_photometry_catalog_dataproduct_group(self):
        transit_dp_group_name = self.get_transit_name()

        self.photometry_catalog_dataproduct_group = DataProductGroup(
            name=transit_dp_group_name
        )
        self.photometry_catalog_dataproduct_group.save()

    def get_reduced_data_products_and_check_pipeline_finished(
        self,
    ) -> list:
        all_products = IAGFacility().data_products(
            self.observation_record.observation_id
        )
        all_products = list(
            filter(lambda prod: prod["imagetype"] == "object", all_products)
        )
        all_products_reduced = list(
            filter(
                lambda prod: prod["rlevel"] == self.CALIBRATED_REDUCTION_LEVEL_INT,
                all_products,
            )
        )

        if not len(all_products_reduced) * 2 >= len(all_products):
            print(
                f"Not all images have been calibrated yet "
                f"({len(all_products_reduced)} reduced images and {len(all_products)} total object images)."
            )
            raise Exception("Reduction pipeline not finished.")

        products_sorted = sorted(all_products_reduced, key=lambda prod: prod["created"])
        return products_sorted

    def create_all_lightcurves_and_fluxrad50_dataproduct(self):
        """Processes the data products in the data group. Creates
        - one of type "transit_all_light_curves" which contains all potential reference star lightcurves that fulfill
            some basic quality criteria (columns are numbers 0, 1, 2...) and a column "target" for the target star flux.

        :raises ValueError if data_product_group contains no DataProducts or data_product_types
        other than "photometry_catalog"s.
        """

        transit_photometry_catalog_group = TransitPhotometryCatalogGroup(
            self.photometry_catalog_dataproduct_group
        )
        image_catalogs = transit_photometry_catalog_group.load_data()

        print(f"Matching on  {len(image_catalogs)} downloaded image catalogs.")

        target_coord = self.get_target_coord()
        matcher = PhotometryCatalogMatcher(image_catalogs, target_coord)
        self.all_light_curves_df = matcher.get_target_and_ref_stars_light_curves_df()
        self.all_light_curves_dp = self.save_all_lightcurves_dataproduct_and_file()
        self.fluxrad50_df = matcher.fluxrad50_df
        self.fluxrad50_dp = self.save_fluxrad50_dataproduct_and_file()
        self.ref_star_catalog_dp = self.save_ref_star_catalog_dataproduct_and_file(
            matcher.ref_catalog
        )

    def save_all_lightcurves_dataproduct_and_file(self) -> DataProduct:
        product_id = f"{self.identifier}_all"
        dp = DataProduct.objects.create(
            extra_data=self.identifier,
            target=self.observation_record.target,
            observation_record=self.observation_record,
            data_product_type="transit_all_light_curves",
        )
        dfile = ContentFile(self.all_light_curves_df.to_csv())
        dp.data.save(
            product_id + ".csv",
            dfile,
        )
        dp.save()
        return dp

    def save_fluxrad50_dataproduct_and_file(self) -> DataProduct:
        product_id = f"{self.identifier}_fluxrad50"
        dp = DataProduct.objects.create(
            product_id=product_id,
            extra_data=self.identifier,
            target=self.observation_record.target,
            observation_record=self.observation_record,
            data_product_type="fluxrad50",
        )
        dfile = ContentFile(self.fluxrad50_df.to_csv())
        dp.data.save(
            product_id + ".csv",
            dfile,
        )
        dp.save()
        return dp

    def create_image_photometry_catalog_dataproduct_and_add_to_group(self, product):

        dp = DataProduct.objects.create(
            target=self.observation_record.target,
            observation_record=self.observation_record,
            data_product_type="image_photometry_catalog",
        )
        dp.group.add(self.photometry_catalog_dataproduct_group)
        dp.save()
        return dp

    def get_transit_name(self):
        observation_record = self.observation_record
        try:
            transit_dp_group_name = observation_record.parameters["name"]
        except:
            try:
                transit_number = observation_record.parameters["transit"]
            except:
                transit_number = "unknown"
            transit_dp_group_name = (
                f"Target {observation_record.target.name}, transit #{transit_number}"
            )

        return transit_dp_group_name.replace(" ", "_")

    def download_and_save_dataproduct_file(self, dp, product):
        product_data, time = self.attempt_image_catalog_download(product, n_attempts=3)
        df = self.get_catalog_dataframe_from_catalog_and_time(product_data, time)
        dfile = ContentFile(df.to_csv())
        dp.data.save(product["filename"].replace(".fits.gz", ".csv"), dfile)
        dp.save()

    def attempt_image_catalog_download(self, product, n_attempts) -> (dict, Time):
        for attempts in range(n_attempts):
            try:
                product_data = requests.get(
                    product["url"].replace("download", "catalog"),
                    headers=IAGFacility().archive_headers(),
                ).content
                time_str = product["created"]
                break
            except Exception as e:
                print(
                    f"Data request to {product['url']} failed due to '{e}'. {2 - attempts} attempts left..."
                )
                time.sleep(0.5)
        else:
            raise Exception(
                f"Couldn't download data from {product['url']} after {n_attempts} due to '{e}'."
            )

        return product_data, Time(time_str)

    def get_catalog_dataframe_from_catalog_and_time(self, product_data, time: Time):
        df = pd.read_csv(io.StringIO(product_data.decode("utf-8")))

        # barycentric correction
        target_coord = SkyCoord(
            self.observation_record.target.ra * u.deg,
            self.observation_record.target.dec * u.deg,
        )
        ltt = time.light_travel_time(
            target_coord, kind="barycentric", location=self.get_earth_location()
        )
        ltt_in_days = ltt / 60 / 60 / 24
        time_barycentric: Time = time + ltt_in_days

        df["time"] = time_barycentric.jd
        return df

    def clean_up_image_photometry_catalog_dataproduct_group(self):
        dp_group = self.photometry_catalog_dataproduct_group
        if dp_group is not None:
            for dp in dp_group.dataproduct_set.all():
                os.remove(dp.data.path)
                dp.delete()
            dp_group.delete()

    def get_target_coord(self):
        target = self.observation_record.target
        target_coord = SkyCoord(target.ra * u.deg, target.dec * u.deg)
        return target_coord

    def lock_observation_record_for_analysis(self):
        self.observation_record.status = "ANALYSING"
        self.observation_record.save()

    def unlock_observation_record_from_analysis(self):
        self.observation_record.status = "COMPLETED"
        self.observation_record.save()

    def get_earth_location(self):
        lat, lon, height = self.get_earth_location_coordinates()
        earth_location = EarthLocation(
            lat=lat * u.deg, lon=lon * u.deg, height=height * u.m
        )
        return earth_location

    def get_earth_location_coordinates(self):
        try:
            instrument = self.observation_record.parameters["instrument_type"]
        except KeyError:
            # if instrument_type not in parameters, use goettingen camera
            instrument = "0M5 SBIG6303E"
        lat = COORDS_BY_INSTRUMENT[instrument]["latitude"]
        lon = COORDS_BY_INSTRUMENT[instrument]["longitude"]
        height = COORDS_BY_INSTRUMENT[instrument]["elevation"]
        return lat, lon, height

    def save_ref_star_catalog_dataproduct_and_file(self, ref_catalog) -> DataProduct:
        product_id = f"{self.identifier}_reference_star_catalog"
        dp = DataProduct.objects.create(
            product_id=product_id,
            extra_data=self.identifier,
            target=self.observation_record.target,
            observation_record=self.observation_record,
            data_product_type="reference_star_catalog",
        )
        dfile = ContentFile(ref_catalog.to_csv())
        dp.data.save(
            product_id + ".csv",
            dfile,
        )
        dp.save()
        return dp
