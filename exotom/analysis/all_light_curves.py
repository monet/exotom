import copy
import json

import pandas as pd
import numpy as np
from astropy.coordinates import SkyCoord
import astropy.units as u

from django.core.files.base import ContentFile
from scipy import optimize, stats
from tom_dataproducts.models import DataProduct
from tom_targets.models import Target

from exotom.analysis.light_curve import FitResult


class AllLightCurves:
    def __init__(
        self, all_lightcurves_dataproduct: DataProduct, fluxrad50_dp: DataProduct
    ):

        self.all_lightcurves_dataproduct = all_lightcurves_dataproduct
        self.check_all_lightcurves_dataproduct_validity()

        self.fluxrad50_dp = fluxrad50_dp
        self.fluxrad50_df = pd.read_csv(
            self.fluxrad50_dp.data.path,
            index_col=["time", "star"],
        )

        self.observation_record = self.all_lightcurves_dataproduct.observation_record
        self.target: Target = self.all_lightcurves_dataproduct.target
        self.target_coord = SkyCoord(self.target.ra * u.deg, self.target.dec * u.deg)

        self.all_light_curves_df = pd.read_csv(
            self.all_lightcurves_dataproduct.data.path,
            index_col=["time", "star", "aperture"],
        )

        self.light_curve_name = self.get_lightcurve_name()
        print(f"Using light curve name {self.light_curve_name}")

        # objects that get created later
        self.best_light_curve_df = None
        self.best_light_curves_dp = None

        self.best_loss_sliding_window_std = None
        self.best_loss_fwhm_correlation_p = None

    def get_lightcurve_name(self):
        return self.all_lightcurves_dataproduct.extra_data

    def check_all_lightcurves_dataproduct_validity(self):
        if (
            self.all_lightcurves_dataproduct.data_product_type
            != "transit_all_light_curves"
        ):
            raise ValueError(
                f"all_lightcurves_dataproduct doesnt have type "
                f"transit_all_light_curves but {self.all_lightcurves_dataproduct.data_product_type}."
            )

        try:
            path = self.all_lightcurves_dataproduct.data.path
            if path is None or path == "":
                raise Exception()
        except:
            raise ValueError(
                f"all_lightcurves_dataproduct ({self.all_lightcurves_dataproduct}) does not have well defined"
                f" DataProduct.data.path"
            )

    def create_best_lightcurve(self, method=""):
        """Processes the data products in the data group. Creates three DataProducts
        - one of type "transit_all_light_curves" which contains all potential reference star lightcurves that fulfill
            some basic quality criteria (columns are numbers 0, 1, 2...) and a column "target" for the target star flux.
        - one of type  "transit_best_light_curves" which contains a selection of reference stars which give the best
            target relativ light curve (in columns "target_rel").
        These dataproducts have an associated csv file.
        - one dataproduct which shows and image of the transit with associated jpg file.

        :raises ValueError if data_product_group contains no DataProducts or data_product_types
        other than "photometry_catalog"s.
        """

        # default method
        if method == "":
            method = "outlier_removal_normaltest_filter_all_ref_stars"

        print(
            f"Transit processing all lightcurves data product: {self.all_lightcurves_dataproduct}"
            f" with method {method}"
        )

        self.find_best_lightcurve(method=method)
        self.save_best_lightcurves_as_dataproduct(method)

        return self.best_light_curves_dp

    def find_best_lightcurve(self, method=""):
        lc_optimizer = LightcurveOptimizer(
            self.all_light_curves_df, self.target_coord, self.fluxrad50_df
        )
        self.best_light_curve_df = (
            lc_optimizer.get_best_relative_transit_light_curve_dataframe(method=method)
        )
        self.best_loss_sliding_window_std = lc_optimizer.loss_sliding_window_std
        self.best_loss_fwhm_correlation_p = lc_optimizer.loss_fwhm_correlation_p

    def save_best_lightcurves_as_dataproduct(self, method):
        self.best_light_curves_dp = self.save_dataframe_as_dataproduct_and_csv_file(
            self.best_light_curve_df,
            product_id=self.light_curve_name + "_best",
            method=method,
            data_product_type="transit_best_light_curves",
        )

    def save_dataframe_as_dataproduct_and_csv_file(
        self, df, product_id, method, data_product_type
    ) -> DataProduct:

        dp = DataProduct.objects.create(
            extra_data=json.dumps(
                {
                    "light_curve_name": self.light_curve_name,
                    "light_curve_optimization_method": method,
                }
            ),
            target=self.target,
            observation_record=self.observation_record,
            data_product_type=data_product_type,
        )
        dfile = ContentFile(df.to_csv())
        dp.data.save(
            product_id + ".csv",
            dfile,
        )
        dp.save()
        return dp


sl = pd.IndexSlice


class LightcurveOptimizer:
    def __init__(
        self,
        all_light_curves_df: pd.DataFrame,
        target_coord: SkyCoord,
        fluxrad50_df: pd.DataFrame,
    ):

        self.all_light_curves_df: pd.DataFrame = all_light_curves_df
        self.target_coord = target_coord
        self.fluxrad50_df = fluxrad50_df

        self.times = self.all_light_curves_df.index.levels[0]
        self.stars = self.all_light_curves_df.index.levels[1]
        self.apertures = self.all_light_curves_df.index.levels[2]
        all_stars = list(self.stars)
        all_stars.remove("target")
        self.ref_star_ids = all_stars

        self.fwhm_detrended = self.make_fwhm_detrended()

        self.lowest_snr_aperture = None
        self.optimized_aperture = None

        self.loss_sliding_window_std = None
        self.loss_fwhm_correlation_p = None

    def make_fwhm_detrended(self):
        fluxrad_50 = self.fluxrad50_df.median(level="time")
        xs = np.arange(len(fluxrad_50))
        poly = np.polynomial.polynomial.Polynomial.fit(
            xs, fluxrad_50.values.flatten(), 3
        )
        fit_data = poly(xs)
        fluxrad_50.loc[:, :] = fluxrad_50.loc[:, :] - fit_data.reshape(-1, 1)
        return fluxrad_50

    def get_best_relative_transit_light_curve_dataframe(
        self, method="brightest_ref_star"
    ) -> (pd.DataFrame, FitResult):
        """Filter out ref stars that are noisy from light_curves_df.
        :returns pd.DataFrame that contains the lightcurves of good reference stars and target
        and relative light curve of target
        """
        (
            self.lowest_snr_aperture,
            best_aperture_index,
        ) = self.find_aperture_with_lowest_signal_to_noise_ratio()

        (
            optimized_df,
            self.optimized_aperture,
        ) = self.optimize_aperture_and_ref_star_selection(
            best_aperture_index, method=method
        )

        df_with_rel_lightcurve = (
            self.create_or_update_target_relative_lightcurve_column_and_errors(
                optimized_df
            )
        )

        self.loss_sliding_window_std = sliding_window_std(
            df_with_rel_lightcurve.loc[sl[:, "target_rel", :], "flux"]
        )
        self.loss_fwhm_correlation_p = fwhm_correlation_p(
            df_with_rel_lightcurve.loc[sl[:, "target_rel", :], "flux"].values.flatten(),
            self.fwhm_detrended.loc[
                df_with_rel_lightcurve.index.levels[0], :
            ].values.flatten(),
        )

        return df_with_rel_lightcurve

    def find_aperture_with_lowest_signal_to_noise_ratio(self):
        rel_errs = {}
        for aper in self.apertures:
            ys = self.all_light_curves_df.loc[sl[:, :, aper], "flux"]
            errs = self.all_light_curves_df.loc[sl[:, :, aper], "error"]
            rel_errors = errs / ys
            rel_errs[aper] = np.mean(rel_errors)

        print("Mean rel errors by aperture", rel_errs)

        best_aper_index = np.argmin(np.array(list(rel_errs.values())))
        best_aper = self.apertures[best_aper_index]
        print(f"Best aper by mean rel error: {best_aper}")

        return best_aper, best_aper_index

    def optimize_aperture_and_ref_star_selection(self, best_aperture_index, method=""):

        dfs, losses = [], []
        print("Looping over apertures to find the one with best metrics...")
        for aperture in self.apertures[best_aperture_index:]:
            print(f"Calculating aper={aperture}...")
            df, loss = self.optimize_ref_star_selection(aperture, method=method)
            dfs.append(df)
            losses.append(loss)

        min_loss_index = np.argmin(losses)
        return dfs[min_loss_index], self.apertures[best_aperture_index + min_loss_index]

    def optimize_ref_star_selection(self, aperture, method="full"):
        if method == "brightest_ref_star":
            return self.optimize_ref_star_selection_method_brightest_ref_star(aperture)
        elif method == "all_ref_stars":
            return self.optimize_ref_star_selection_method_all_ref_stars(aperture)
        elif method == "outlier_removal_all_ref_stars":
            return (
                self.optimize_ref_star_selection_method_outlier_removal_all_ref_stars(
                    aperture
                )
            )
        elif method == "outlier_removal_normaltest_filter_all_ref_stars":
            return self.optimize_ref_star_selection_method_outlier_removal_normaltest_filter_all_ref_stars(
                aperture
            )
        elif method == "full":
            return self.optimize_ref_star_selection_method_full(aperture)
        else:
            raise ValueError(
                f"Ref star optimization method '{method}' not recognized. Must be from"
                f"[brightest_ref_star, all_ref_stars, outlier_removal_all_ref_stars, "
                f"outlier_removal_normaltest_filter_all_ref_stars, full]"
            )

    def optimize_ref_star_selection_method_brightest_ref_star(self, aperture):
        df_ref_stars: pd.DataFrame = copy.deepcopy(
            self.all_light_curves_df.loc[sl[:, :, aperture], "flux"]
        )
        df_ref_stars.index = df_ref_stars.index.remove_unused_levels()

        df_optimized, loss = self.get_brightest_ref_star_lightcurve(df_ref_stars)

        return df_optimized, loss

    def get_brightest_ref_star_lightcurve(self, df):
        ref_star_ids = [star for star in df.index.levels[1] if star != "target"]

        target_flux = df.loc[sl[:, "target", :]]
        target_flux.index = target_flux.index.remove_unused_levels()
        ref_star_fluxes = df.loc[sl[:, ref_star_ids, :]]
        ref_star_fluxes.index = ref_star_fluxes.index.remove_unused_levels()

        brightest_idx = ref_star_fluxes.sum(level="star").idxmax()

        loss = loss_function(
            np.array((1,)),
            target_flux,
            ref_star_fluxes.loc[sl[:, brightest_idx, :]],
            self.fwhm_detrended.loc[ref_star_fluxes.index.levels[0], :],
        )

        new_star_ids = [brightest_idx, "target"]

        df = df.loc[sl[:, new_star_ids, :]]
        return df, loss

    def optimize_ref_star_selection_method_all_ref_stars(self, aperture):
        df_flux: pd.DataFrame = copy.deepcopy(
            self.all_light_curves_df.loc[sl[:, :, aperture], "flux"]
        )
        df_flux.index = df_flux.index.remove_unused_levels()

        ref_star_ids = [star for star in df_flux.index.levels[1] if star != "target"]

        target_flux = df_flux.loc[sl[:, "target", :]]
        target_flux.index = target_flux.index.remove_unused_levels()
        ref_star_fluxes = df_flux.loc[sl[:, ref_star_ids, :]]
        ref_star_fluxes.index = ref_star_fluxes.index.remove_unused_levels()

        loss = loss_function(
            np.ones(len(ref_star_ids)),
            target_flux,
            ref_star_fluxes,
            self.fwhm_detrended.loc[ref_star_fluxes.index.levels[0], :],
        )

        return df_flux, loss

    def optimize_ref_star_selection_method_outlier_removal_all_ref_stars(
        self, aperture
    ):
        df_ref_stars: pd.DataFrame = copy.deepcopy(
            self.all_light_curves_df.loc[sl[:, :, aperture], "flux"]
        )
        df_ref_stars.index = df_ref_stars.index.remove_unused_levels()

        df_outliers_removed = self.remove_outlier_frames_and_ref_stars(df_ref_stars)
        ref_star_ids = [
            star for star in df_outliers_removed.index.levels[1] if star != "target"
        ]

        target_flux = df_outliers_removed.loc[sl[:, "target", :]]
        target_flux.index = target_flux.index.remove_unused_levels()
        ref_star_fluxes = df_outliers_removed.loc[sl[:, ref_star_ids, :]]
        ref_star_fluxes.index = ref_star_fluxes.index.remove_unused_levels()

        loss = loss_function(
            np.ones(len(ref_star_ids)),
            target_flux,
            ref_star_fluxes,
            self.fwhm_detrended.loc[ref_star_fluxes.index.levels[0], :],
        )

        return df_outliers_removed, loss

    def optimize_ref_star_selection_method_outlier_removal_normaltest_filter_all_ref_stars(
        self, aperture
    ):
        df_ref_stars: pd.DataFrame = copy.deepcopy(
            self.all_light_curves_df.loc[sl[:, :, aperture], "flux"]
        )
        df_ref_stars.index = df_ref_stars.index.remove_unused_levels()

        df_outliers_removed = self.remove_outlier_frames_and_ref_stars(df_ref_stars)

        df_filtered_by_normaltest = self.filter_ref_stars_by_normaltest(
            df_outliers_removed
        )

        ref_star_ids = [
            star
            for star in df_filtered_by_normaltest.index.levels[1]
            if star != "target"
        ]

        target_flux = df_filtered_by_normaltest.loc[sl[:, "target", :]]
        target_flux.index = target_flux.index.remove_unused_levels()
        ref_star_fluxes = df_filtered_by_normaltest.loc[sl[:, ref_star_ids, :]]
        ref_star_fluxes.index = ref_star_fluxes.index.remove_unused_levels()

        loss = loss_function(
            np.ones(len(ref_star_ids)),
            target_flux,
            ref_star_fluxes,
            self.fwhm_detrended.loc[ref_star_fluxes.index.levels[0], :],
        )

        return df_filtered_by_normaltest, loss

    def optimize_ref_star_selection_method_full(self, aperture):
        # get light curve
        df_ref_stars: pd.DataFrame = copy.deepcopy(
            self.all_light_curves_df.loc[sl[:, :, aperture], "flux"]
        )
        df_ref_stars.index = df_ref_stars.index.remove_unused_levels()

        df_outliers_removed = self.remove_outlier_frames_and_ref_stars(df_ref_stars)
        df_filtered_by_normaltest = self.filter_ref_stars_by_normaltest(
            df_outliers_removed
        )
        (
            df_optimized,
            loss,
        ) = self.find_best_ref_star_selection_with_continuous_optimization(
            df_filtered_by_normaltest
        )

        return df_optimized, loss

    def remove_outlier_frames_and_ref_stars(
        self,
        df,
        n_sigma=2.5,
        max_removed_frame_fraction=0.4,
        max_removed_star_fraction=0.8,
        verbose=False,
    ):
        print("Simultaneous outlier removal of ref stars and frames.")
        remove_at_most_frames = int(len(self.times) * max_removed_frame_fraction)
        remove_at_most_stars = int(len(self.ref_star_ids) * max_removed_star_fraction)

        n_frames_removed = 0
        n_stars_removed = 0

        while True:

            ref_star_mask = np.array([idx[1] != "target" for idx in df.index])
            df_ref_stars = df.loc[sl[:, ref_star_mask, :]]

            if verbose:
                print(
                    f"{len(df_ref_stars.index.levels[0])} frames, "
                    f"{len(df_ref_stars.index.levels[1])} stars"
                )

            # each star normed/made comparable
            ref_star_fluxes_norm_frame_filter = df_ref_stars / df_ref_stars.median(
                level="star"
            )
            # each frame normed/made comparable
            ref_star_fluxes_norm_star_filter = df_ref_stars / df_ref_stars.median(
                level="time"
            )

            stds_frame_filter = ref_star_fluxes_norm_frame_filter.std(level="time")
            stds_star_filter = ref_star_fluxes_norm_star_filter.std(level="star")

            stds_frame_filter_in_stds = (
                stds_frame_filter - np.std(stds_frame_filter)
            ) / np.mean(stds_frame_filter)
            stds_star_filter_in_stds = (
                stds_star_filter - np.std(stds_star_filter)
            ) / np.mean(stds_star_filter)

            frame_metric: pd.DataFrame = stds_frame_filter_in_stds
            star_metric: pd.DataFrame = stds_star_filter_in_stds

            worst_frame_idx = frame_metric.idxmax()
            worst_frame_metric = frame_metric.loc[worst_frame_idx]
            worst_star_idx = star_metric.idxmax()
            worst_star_metric = star_metric[worst_star_idx]

            removed = False

            # check if worst frame is overall worst outlier
            if n_frames_removed <= remove_at_most_frames:
                if worst_frame_metric > n_sigma and (
                    worst_frame_metric > worst_star_metric
                    or n_stars_removed == remove_at_most_stars
                ):
                    print(f"Removing frame {worst_frame_idx}")
                    n_frames_removed += 1
                    df.drop(worst_frame_idx, level="time", inplace=True)
                    df.index = df.index.remove_unused_levels()
                    removed = True

            # check if worst star is overall worst outlier
            if n_stars_removed <= remove_at_most_stars:
                if worst_star_metric > n_sigma and (
                    worst_frame_metric <= worst_star_metric
                    or n_frames_removed == remove_at_most_frames
                ):

                    print(f"Removing star {worst_star_idx}")
                    n_stars_removed += 1
                    df.drop(worst_star_idx, level="star", inplace=True)
                    df.index = df.index.remove_unused_levels()
                    removed = True

            if not removed:
                break

        return df

    def filter_ref_stars_by_normaltest(
        self, df, min_normaltest_p=0.01, max_remove_fraction=0.5
    ):

        ref_star_fluxes = copy.deepcopy(df)
        ref_star_ids = list(ref_star_fluxes.index.levels[1])
        ref_star_ids.remove("target")
        ref_star_fluxes = ref_star_fluxes.loc[sl[:, ref_star_ids, :]]
        ref_star_fluxes.index = ref_star_fluxes.index.remove_unused_levels()

        at_least_keep_n_stars = max(len(ref_star_ids) * (1 - max_remove_fraction), 1)

        n_stars_filtered_out = 0
        while True:
            relative_lc = ref_star_fluxes.loc[sl[:, :, :]] / ref_star_fluxes.loc[
                sl[:, :, :]
            ].sum(level="time")

            relative_lc_array = relative_lc.values.reshape(
                list(map(len, relative_lc.index.levels))
            )
            if len(relative_lc_array) < 20:
                print(
                    f"Can't do normaltest filtering because there are fewer than 20 frames left."
                    f"{len(relative_lc_array)} times left: {relative_lc.index.levels[0]}"
                )
                break

            _, ps = stats.normaltest(
                relative_lc_array, axis=0
            )  # do normaltest over time axis (=axis 0)

            min_p_arg = np.argmin(ps)
            min_p_idx = relative_lc.index.levels[1][min_p_arg]
            min_p = ps[min_p_arg]

            if (
                min_p < min_normaltest_p
                and len(ref_star_fluxes.index.levels[1]) > at_least_keep_n_stars
            ):
                ref_star_fluxes.drop(min_p_idx, level="star", inplace=True)
                ref_star_fluxes.index = ref_star_fluxes.index.remove_unused_levels()

                n_stars_filtered_out += 1
            else:
                break

        new_ref_ids = list(ref_star_fluxes.index.levels[1])
        print(
            f"{n_stars_filtered_out} stars filtered out because normaltest.p < min_normaltest_p={min_normaltest_p}."
        )
        print(f"Final stars after normaltest filter {new_ref_ids}")
        new_ref_ids.append("target")
        filtered_df = df.loc[sl[:, new_ref_ids, :]]
        filtered_df.index = filtered_df.index.remove_unused_levels()

        return filtered_df

    def find_best_ref_star_selection_with_continuous_optimization(
        self,
        df,
        max_use_n_ref_stars=150,
        verbose=False,
        global_optimization_max_iterations=500,
    ):
        eps = 1e-8

        all_ref_star_ids = [star for star in df.index.levels[1] if star != "target"]
        target_flux = df.loc[sl[:, "target", :]]
        target_flux.index = target_flux.index.remove_unused_levels()
        ref_star_fluxes = df.loc[sl[:, all_ref_star_ids, :]]
        ref_star_fluxes.index = ref_star_fluxes.index.remove_unused_levels()

        # limit to brightest max_use_n_ref_stars stars
        ref_star_ids = list(
            ref_star_fluxes.mean(level="star")
            .sort_values(ascending=False)
            .iloc[:max_use_n_ref_stars]
            .index
        )

        fwhm = self.fwhm_detrended.loc[ref_star_fluxes.index.levels[0], :]

        bounds = np.array([(0.0, 1.0)] * len(ref_star_ids))

        print(
            "Performing global optimization (optimize.differential_evolution) "
            "on continuous space of ref star light curve inclusion..."
        )
        res = optimize.differential_evolution(
            loss_function,
            args=(target_flux, ref_star_fluxes.loc[sl[:, ref_star_ids, :]], fwhm),
            bounds=bounds,
            polish=False,
            maxiter=global_optimization_max_iterations,
            seed=4,
        )
        print("Global optimization result: ", res)

        additional_refstar_fluxes_sum = np.zeros(
            shape=(len(target_flux.index.levels[0])), dtype=float
        )

        beta = res.x

        include_ref_stars = []
        exclude_ref_stars = []

        if len(ref_star_ids) != 0:
            # optimize locally
            res = optimize.minimize(
                loss_function,
                x0=beta,
                args=(
                    target_flux,
                    ref_star_fluxes.loc[sl[:, ref_star_ids, :]],
                    fwhm,
                    additional_refstar_fluxes_sum,
                ),
                method="L-BFGS-B",
                bounds=bounds,
            )
            beta = res.x
            last_loss = res.fun
        else:
            last_loss = loss_function(
                np.array([0]),
                target_flux,
                pd.DataFrame(np.array([0])),
                fwhm,
                additional_refstar_fluxes_sum,
            )

        while len(ref_star_ids) != 0:
            # select star that is going to be decided, namely star whose beta is closest to 0.5
            distance_from_one_half = (beta - 0.5) ** 2
            min_dist_from_one_half, min_dist_from_one_half_index = (
                np.min(distance_from_one_half),
                np.argmin(distance_from_one_half),
            )

            if verbose:
                print(
                    f"Star to be decided: {ref_star_ids[min_dist_from_one_half_index]} with dist from .5 {min_dist_from_one_half}"
                )

            # try exclusion
            try_star_multiplier = 0.0
            beta[min_dist_from_one_half_index] = try_star_multiplier
            bounds[min_dist_from_one_half_index] = (0.0, eps)
            # make sure at least one ref star is included
            if not (beta == 0.0).all() or (additional_refstar_fluxes_sum != 0.0).all():
                res = optimize.minimize(
                    loss_function,
                    x0=beta,
                    args=(
                        target_flux,
                        ref_star_fluxes.loc[sl[:, ref_star_ids, :]],
                        fwhm,
                        additional_refstar_fluxes_sum,
                    ),
                    method="L-BFGS-B",
                    bounds=bounds,
                )
                loss_exclusion = res.fun
                beta_exclusion = res.x
            else:
                loss_exclusion = np.inf
                beta_exclusion = beta

            # try inclusion
            try_star_multiplier = 1.0

            beta[min_dist_from_one_half_index] = try_star_multiplier
            bounds[min_dist_from_one_half_index] = (1.0 - eps, 1.0)
            res = optimize.minimize(
                loss_function,
                x0=beta,
                args=(
                    target_flux,
                    ref_star_fluxes.loc[sl[:, ref_star_ids, :]],
                    fwhm,
                    additional_refstar_fluxes_sum,
                ),
                method="L-BFGS-B",
                bounds=bounds,
            )
            loss_inclusion = res.fun
            beta_inclusion = res.x

            if loss_inclusion < loss_exclusion:
                print(f"Including star {ref_star_ids[min_dist_from_one_half_index]}.")
                last_loss = loss_inclusion
                beta = beta_inclusion
                additional_refstar_fluxes_sum += ref_star_fluxes.loc[
                    sl[:, ref_star_ids[min_dist_from_one_half_index]]
                ]
                include_ref_stars.append(ref_star_ids.pop(min_dist_from_one_half_index))
                bounds = np.delete(bounds, min_dist_from_one_half_index, axis=0)
                beta = np.delete(beta, min_dist_from_one_half_index, axis=0)
            else:
                # reset to exclusion
                print(f"Excluding star {ref_star_ids[min_dist_from_one_half_index]}.")
                last_loss = loss_exclusion
                beta = beta_exclusion
                exclude_ref_stars.append(ref_star_ids.pop(min_dist_from_one_half_index))
                bounds = np.delete(bounds, min_dist_from_one_half_index, axis=0)
                beta = np.delete(beta, min_dist_from_one_half_index, axis=0)

        print(f"Final included stars: {include_ref_stars}")
        print(f"Final excluded: {exclude_ref_stars}")
        new_ref_ids = include_ref_stars
        new_ref_ids.append("target")
        df = df.loc[sl[:, new_ref_ids, :]]

        return df, last_loss

    def create_or_update_target_relative_lightcurve_column_and_errors(
        self, optimized_df
    ):
        # create df with flux and errors
        errors = self.all_light_curves_df.loc[optimized_df.index, "error"]
        optimized_df_with_errors = pd.concat(
            [
                optimized_df,
                errors,
            ],
            axis="columns",
        )
        optimized_df_with_errors.index = (
            optimized_df_with_errors.index.remove_unused_levels()
        )

        # split target and ref stars
        optimized_ref_star_ids = list(optimized_df_with_errors.index.levels[1])
        optimized_ref_star_ids.remove("target")
        ref_star_fluxes_and_errors = optimized_df_with_errors.loc[
            sl[:, optimized_ref_star_ids, :], :  # ['flux', 'error']
        ]
        target_flux_and_error = optimized_df_with_errors.loc[
            sl[:, "target", :], ["flux", "error"]
        ]

        # calc sum relative normed light curve
        ref_star_fluxes_sum = ref_star_fluxes_and_errors.loc(axis="columns")[
            "flux"
        ].sum(level="time")
        target_flux = target_flux_and_error.loc(axis="columns")["flux"]
        rel_lc = target_flux / ref_star_fluxes_sum
        rel_normed_lc = rel_lc / np.mean(rel_lc)

        # calc error of rel normed light curve through gaussian error propagation
        target_error = target_flux_and_error.loc(axis="columns")["error"]
        ref_star_errors = ref_star_fluxes_and_errors.loc(axis="columns")["error"]
        rel_normed_lc_error: pd.Series = np.sqrt(
            target_error ** 2 / (ref_star_fluxes_sum ** 2)
            + (
                ref_star_errors ** 2 * target_flux ** 2 / (ref_star_fluxes_sum ** 4)
            ).sum(level="time")
        ) / np.mean(rel_lc)
        rel_normed_lc_error.rename("error", inplace=True)

        # concat rel flux and errors
        rel_normed_lc_flux_and_error = pd.concat(
            [rel_normed_lc, rel_normed_lc_error], axis="columns"
        )
        rel_normed_lc_flux_and_error.rename(
            {"target": "target_rel"}, level="star", inplace=True
        )

        # concat ref star light curves and rel light curve with errors
        optimized_df_with_rel_lc = pd.concat(
            [optimized_df_with_errors, rel_normed_lc_flux_and_error], axis="index"
        )
        optimized_df_with_rel_lc.index = (
            optimized_df_with_rel_lc.index.remove_unused_levels()
        )

        return optimized_df_with_rel_lc


def loss_function(
    beta,
    target_flux: pd.DataFrame,
    ref_star_fluxes: pd.DataFrame,
    fwhm_detrended,
    additional_refstar_fluxes_sum=0.0,
):
    ref_star_sum_flux = (
        np.sum(ref_star_fluxes.values.reshape((-1, len(beta))) * beta, axis=1)
        + additional_refstar_fluxes_sum
    )

    rel_flux = target_flux / ref_star_sum_flux
    norm_rel_flux = rel_flux / np.mean(rel_flux)

    # order of 0.005, variations ~ 0.0025
    median_sliding_window_std = sliding_window_std(norm_rel_flux)

    ### inline of function fwhm_correlation_p() for efficiency
    # order of 1e-20 to 1
    _, fwhm_correlation_p_value = stats.pearsonr(
        norm_rel_flux.values, fwhm_detrended.values.flatten()
    )
    # from 0 to 20, variations ~ 5
    log_fwhm_correlation_p = -np.log10(fwhm_correlation_p_value)
    # variations ~ 1
    log_fwhm_correlation_p_scaled = log_fwhm_correlation_p / 5
    # variaions ~ 0.0025
    log_fwhm_correlation_p_scaled = log_fwhm_correlation_p_scaled * 0.0025

    loss = median_sliding_window_std + log_fwhm_correlation_p_scaled
    return loss


def sliding_window_std(rel_flux, window_length_in_hours=0.5):

    window_length_in_days = window_length_in_hours / 24

    times = rel_flux.index.levels[0]
    window_length = len(times[times < times[0] + window_length_in_days])

    total_length = len(rel_flux)
    if window_length > total_length:
        return 0
        # raise ValueError(
        #     f"window length {window_length} larger than last dimension of rel_flux {total_length}"
        # )

    sliding_window = np.lib.stride_tricks.sliding_window_view(
        rel_flux.values,
        window_shape=window_length,
        axis=0,
    )

    stds = np.std(sliding_window, axis=1)
    return np.median(stds)


def fwhm_correlation_p(norm_rel_flux, fwhm_detrended):
    _, fwhm_correlation_p = stats.pearsonr(norm_rel_flux, fwhm_detrended)
    return fwhm_correlation_p
