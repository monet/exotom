from django.core.management.base import BaseCommand
from exotom.analysis.analysis_helper_functions import process_new_observations_command


class Command(BaseCommand):
    help = "Process all new observations that haven't been processed yet"

    def handle(self, *args, **options):
        process_new_observations_command()
