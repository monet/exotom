import traceback

from django.core.management.base import BaseCommand

from tom_iag import iag
from tom_observations.models import ObservationRecord
from exotom.settings import SITES, MAX_EXPOSURES_PER_REQUEST
from exotom.ofi.iagtransit import (
    IAGTransitForm,
    IAGTransitFacility,
    IAGTransitSingleContactForm,
)

from exotom.models import Target, Transit
from exotom.transits import calculate_transits_during_next_n_days
from exotom.exposure_calculator import calculate_exposure_time

import numpy as np

site_names = tuple(SITES.keys())


class Command(BaseCommand):
    help = "Submit all transits that can be observed in the following night to telescope scheduler."

    def add_arguments(self, parser):
        parser.add_argument("--only_n_transits", type=int, default=np.inf)
        parser.add_argument("--sites", type=str, nargs="+", default=site_names)
        parser.add_argument("--contacts", default=False, action="store_true")

    def handle(self, *args, **options):
        submit_only_n_transits = options["only_n_transits"]
        sites = options["sites"]
        contacts = options["contacts"]
        submit_all_transits(sites, submit_only_n_transits, contacts)


def submit_all_transits(
    submit_to_sites=site_names, submit_only_n_transits: int = np.inf, contacts=False
):
    submission_counter = 0

    for submit_to_site in submit_to_sites:
        if submit_to_site not in site_names:
            raise ValueError(
                f"Site '{submit_to_site}' not known. Options are {site_names} (multiple possible)."
            )

    # filter down sites to those that should be submitted to
    sites_to_submit_to = {
        site_name: site_info
        for site_name, site_info in SITES.items()
        if site_name in submit_to_sites
    }

    targets = Target.objects.all().order_by("id")
    instruments = iag.get_instruments()

    for site_name, site_info in sites_to_submit_to.items():
        instrument_type = site_info["instrument"]
        instrument_details = instruments[instrument_type]

        for target in targets:
            transits_for_target = calculate_transits_during_next_n_days(
                target, n_days=1
            )

            for transit in transits_for_target:
                if submission_counter >= submit_only_n_transits:
                    return

                n_submitted = submit_if_observable(
                    transit, site_name, instrument_type, instrument_details, contacts
                )
                submission_counter += n_submitted


def submit_if_observable(
    transit, site_name, instrument_type, instrument_details, contacts: bool
):
    n_submitted = 0
    if not contacts:
        if transit.observable_at_site(site=site_name):
            n_submitted += try_submit(transit, instrument_type, instrument_details)
    elif contacts:
        if transit.ingress_observable_at_site(site_name):
            n_submitted += try_submit(
                transit, instrument_type, instrument_details, contact="INGRESS"
            )
        if transit.egress_observable_at_site(site_name):
            n_submitted += try_submit(
                transit, instrument_type, instrument_details, contact="EGRESS"
            )

    return n_submitted


def try_submit(transit, instrument_type, instrument_details, contact: str = None):
    try:
        submit_observation_to_instrument(
            transit, instrument_type, instrument_details, contact=contact
        )
        return 1
    except Exception as e:
        print(f"Error when submitting transit observation to instrument")
        print(traceback.format_exc())
        return 0


def submit_observation_to_instrument(
    transit: Transit,
    instrument_type: str,
    instrument_details: dict,
    contact: str = None,
):
    observation_data = get_observation_data(
        transit, instrument_type, instrument_details, contact
    )

    if not contact:
        form = IAGTransitForm(initial=observation_data, data=observation_data)
    else:
        form = IAGTransitSingleContactForm(
            initial=observation_data, data=observation_data
        )
    form.is_valid()
    observation_payload = form.observation_payload()

    facility = IAGTransitFacility()

    n_exposures = facility.get_number_of_exposures(observation_payload)
    if n_exposures > MAX_EXPOSURES_PER_REQUEST:
        raise Exception(
            f"{n_exposures} requested which is more than {MAX_EXPOSURES_PER_REQUEST}"
            f" max allowed per request."
        )

    print(
        f"Submitting transit {transit}{contact} with transit id {transit.id} and target id {transit.target_id}"
    )

    observation_ids = facility.submit_observation(observation_payload)

    # create observation record
    for observation_id in observation_ids:
        record = ObservationRecord.objects.create(
            target=transit.target,
            facility="IAGTransit",
            parameters=form.cleaned_data,
            observation_id=observation_id,
        )


def get_observation_data(
    transit: Transit,
    instrument_type: str,
    instrument_details: dict,
    contact: str = None,
) -> dict:
    magnitude = transit.target.targetextra_set.get(key="Mag (TESS)").float_value
    exposure_time = calculate_exposure_time(magnitude, instrument_type)

    contact_string = f" {contact}" if contact else ""
    data = {
        "name": f"{transit.target.name} #{transit.number}{contact_string}",
        "facility": "IAGTransit",
        "instrument_type": instrument_type,
        "transit": transit.number,
        "transit_id": transit.id,
        "target_id": transit.target_id,
        "ipp_value": get_ipp_value(transit, contact),
        "max_airmass": 2.0,  # correspond to alt >= 30°
        "exposure_time": exposure_time,
        "readout_mode": instrument_details["modes"]["readout"]["modes"][0]["code"],
        "filter": instrument_details["optical_elements"]["filters"][0]["code"],
    }
    if contact:
        data["contact"] = contact

    return data


def get_ipp_value(transit: Transit, contact: str = None) -> float:
    if not contact:
        return 1.05
    else:
        return 0.5
