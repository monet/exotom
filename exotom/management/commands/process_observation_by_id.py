from django.core.management.base import BaseCommand
from exotom.analysis.analysis_helper_functions import (
    analyze_observation_record_by_observation_id,
)


class Command(BaseCommand):
    help = "Process all new observations that haven't been processed yet"
    # ./manage.py process_observation_by_id --obs_id 14495 --method brightest_ref_star
    # ./manage.py process_observation_by_id --obs_id 14495 --method all_ref_stars --sparse 10 --force_redownload True
    # ./manage.py process_observation_by_id --obs_id 14495 --method full --sparse 10 --force_redownload True

    def add_arguments(self, parser):
        parser.add_argument("--obs_id", type=int)
        parser.add_argument("--method", type=str, default="")
        parser.add_argument("--sparse", type=int, default=1)
        parser.add_argument("--force_redownload", type=bool, default=False)

    def handle(self, *args, **options):
        obs_id = options["obs_id"]
        use_every_nth_timestep = options["sparse"]
        force_redownload = options["force_redownload"]
        method = options["method"]

        analyze_observation_record_by_observation_id(
            obs_id,
            force_redownload=force_redownload,
            use_every_nth_timestep=use_every_nth_timestep,
            method=method,
        )
