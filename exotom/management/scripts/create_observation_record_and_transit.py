import argparse
import json
import re
from datetime import datetime, timedelta
import pytz

# python ./targets/create_observation_record_and_transit.py --observation_id 2 --target_name 1809  --start_date 2021 3 5 15
from typing import Union, Optional, List

from tom_observations.models import ObservationRecord
from tom_targets.models import Target

from exotom.models import Transit
from exotom.transits import calculate_transits_during_next_n_days

from astropy.time import Time

from tom_iag import iag


def get_request_group_dict_from_request_group_id(request_group_id):
    try:
        response = iag.make_request(
            "GET",
            iag.PORTAL_URL + "/api/requestgroups/{0}".format(request_group_id),
            headers=iag.IAGFacility()._portal_headers(),
        )
        raw_dict = json.loads(response.content.decode("utf-8"))

    except Exception as e:
        print(e)
        print(f"Couldnt request req group id {request_group_id}")
        raise e

    req_group_dict = {}

    obs_name_regex_string = r"(?P<target_name>TOI \d{1,4}\.\d{2}) #(?P<transit_number>\d{1,4}) ?(?P<contact>INGRESS|EGRESS|)"
    obs_name_regex = re.compile(obs_name_regex_string)

    req_group_dict["request_group_id"] = request_group_id

    req = raw_dict["requests"][0]
    config = req["configurations"][0]
    target = config["target"]

    observation_name = raw_dict["name"]
    parsed = obs_name_regex.match(observation_name)
    req_group_dict["target_name"] = parsed["target_name"]
    req_group_dict["transit_number"] = parsed["transit_number"]
    req_group_dict["contact"] = parsed["contact"]

    print(
        observation_name,
        req_group_dict["target_name"],
        req_group_dict["transit_number"],
        req_group_dict["contact"],
    )

    req_group_dict["observation_start"] = req["windows"][0]["start"]
    req_group_dict["observation_end"] = req["windows"][0]["end"]
    req_group_dict["request_id"] = req["id"]
    req_group_dict["exposure_time"] = config["instrument_configs"][0]["exposure_time"]

    return req_group_dict


def create_observation_record_and_transit_from_request_group_id(request_group_id):
    dictt = get_request_group_dict_from_request_group_id(request_group_id)
    res = create_observation_record_and_transit(
        dictt["request_id"],
        dictt["target_name"],
        dictt["transit_number"],
        dictt["contact"],
        datetime.fromisoformat(dictt["observation_start"].replace("Z", "+00:00")),
    )
    return res


def create_observation_record_and_transit(
    observation_id: int,
    target_name: str,
    transit_number: Union[int, str],
    contact: str = "",
    start_date: Optional[Union[List[int], datetime]] = None,
):
    """Create database objects for a transit observation submitted from a different ExoTOM instance.

    When observations were submitted from a different ExoTom instance, the ObservationRecord and Transit
    database objects are missing (assuming the target object already exists).
    So if you want to run the analysis on a different instance you have
    to create the Transit object (done here by calling the calculate_transits_during_next_n_days function)
    and then the ObservationRecord.

    All necessary parameters can be taken from the observation request page or the observation status page.

    :param observation_id: "request id" in the observation portal
    :param target_name: str. part of the target name  (eg "TOI 1809" or "1809") used to find the target object. Should be unique.
    :param transit_number: number of the transit (should be in observation request name)
    :param contact: str = "". "INGRESS" or "EGRESS"
    :param start_date: Optional[Union[int, datetime]] = None. date less than 24 hours before the transit as datetime or [year, month, day, hour]
        (eg [2021, 4, 3, 15]). If not given, now minus 24 hours is used.
    """
    # print(observation_id, target_name, transit_number, contact, start_date)
    transit_number = int(transit_number)

    if start_date is None:
        print("start_date not given, using now - 24 hours")
        start_time = datetime.now() - timedelta(hours=24)
    elif type(start_date) == datetime:
        start_time = start_date
    elif type(start_date) in [list, tuple]:
        start_time = datetime(*start_date)
    else:
        raise TypeError(f"start_date of type {type(start_date)} not allowed.")

    # print(repr(start_time), observation_id, target_name)

    target: Target = Target.objects.get(name__contains=target_name)

    # check if transit number and start time fit
    eps = timedelta(hours=3)
    from_epoch = Time(
        target.targetextra_set.get(key="Epoch (BJD)").float_value
        + transit_number * target.targetextra_set.get(key="Period (days)").float_value
        - target.targetextra_set.get(key="Duration (hours)").float_value / 24 / 2,
        format="jd",
    )
    if not (start_time - eps < from_epoch < start_time + eps):
        raise ValueError(
            f"Transit number and epoch/duration dont fit. "
            f"Given start_time: {start_time}. "
            f"Calculated from epoch & duration: {from_epoch.iso}"
        )

    transit = Transit.objects.filter(
        target=target,
        number=transit_number,
    ).first()
    if transit is None:
        print(
            f"Transit {target} #{transit_number} start time {start_time} not found, creating."
        )
        duration_hours = target.targetextra_set.get(key="Duration (hours)").float_value
        created_transits = calculate_transits_during_next_n_days(
            target,
            n_days=1 + duration_hours / 24 + eps.days,
            start_time=start_time - eps,
        )
        print(created_transits)
        # print(target, transit_number)
        transit: Transit = Transit.objects.get(target=target, number=transit_number)

    observation_record = ObservationRecord.objects.filter(
        target=target,
        observation_id=observation_id,
    ).first()
    if observation_record is None:
        print("ObservationRecord not found, creating.")
        observation_record = ObservationRecord.objects.create(
            target=target,
            facility="IAGTransit",
            observation_id=observation_id,
            parameters={
                "transit": transit_number,
                "transit_id": transit.id,
                "target_id": target.id,
                "name": f"Target {target.name}, transit #{transit.number} " + contact,
            },
        )

    return observation_record, transit


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("--observation_id", type=int, required=True)
    parser.add_argument("--target_name", type=str, required=True)
    parser.add_argument("--transit_number", type=int, required=True)
    parser.add_argument(
        "--start_date", action="store", required=True, nargs=4, type=int
    )

    args = parser.parse_args()

    create_observation_record_and_transit(
        args.observation_id, args.target_name, args.transit_number, args.start_date
    )

    create_observation_record_and_transit(
        observation_id=15225,
        target_name="TOI 1252.01",
        transit_number=589,
        start_date=[2021, 5, 9, 12],
    )

    create_observation_record_and_transit(
        observation_id=14495,
        target_name="TOI 1780.01",
        transit_number=270,
        start_date=[20121, 4, 27, 12],
    )
