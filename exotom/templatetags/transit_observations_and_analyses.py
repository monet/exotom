import datetime

from django import template
from tom_dataproducts.models import DataProduct
from tom_observations.models import ObservationRecord

from exotom.models import Transit, TransitAnalysisResult

register = template.Library()


@register.inclusion_tag("exotom/partials/transit_observations_and_analyses.html")
def transit_observations_and_analyses(transit: Transit):

    potential_transit_obs_records = ObservationRecord.objects.filter(
        target=transit.target
    )
    transit_obs_records = []
    for pot_obs_record in potential_transit_obs_records:
        if int(pot_obs_record.parameters["transit"]) == transit.number:
            transit_obs_records.append(pot_obs_record)

    n_obs_records = len(transit_obs_records)

    observations_and_analyses = {}
    for obs_record in transit_obs_records:
        analysis_results = list(
            TransitAnalysisResult.objects.filter(
                transit=transit, observation_record=obs_record
            )
        )
        observations_and_analyses[obs_record] = analysis_results

    return {
        "n_obs_records": n_obs_records,
        "observations_and_analyses": observations_and_analyses,
    }
