import os

from django.test import TestCase
from django.core.serializers import deserialize
from django.core.files.base import File

from tom_dataproducts.models import DataProduct

from exotom.analysis.all_light_curves import AllLightCurves


class Test(TestCase):
    def setUp(self) -> None:
        self.data_file1 = "exotom/test/test_all_light_curve_data/TOI_1780.01_270_2021-07-22T091507_all.csv"
        self.fluxrad50_path = "exotom/test/test_all_light_curve_data/TOI_1780.01_270_2021-07-22T143511_fluxrad50.csv"

        target2_serialized = '[{"model": "tom_targets.target", "pk": 1347, "fields": {"name": "test_TOI 1780.01", "type": "SIDEREAL", "created": "2021-03-01T12:07:00.175Z", "modified": "2021-03-01T12:07:00.175Z", "ra": 165.16720833333332, "dec": 64.96384444444445, "epoch": null, "parallax": null, "pm_ra": null, "pm_dec": null, "galactic_lng": null, "galactic_lat": null, "distance": null, "distance_err": null, "scheme": "", "epoch_of_elements": null, "mean_anomaly": null, "arg_of_perihelion": null, "eccentricity": null, "lng_asc_node": null, "inclination": null, "mean_daily_motion": null, "semimajor_axis": null, "epoch_of_perihelion": null, "ephemeris_period": null, "ephemeris_period_err": null, "ephemeris_epoch": null, "ephemeris_epoch_err": null, "perihdist": null}}]'
        deserialized_obj = deserialize("json", target2_serialized).__next__()
        deserialized_obj.save()
        self.target1 = deserialized_obj.object

        target1_extras_serialized = '[{"model": "tom_targets.targetextra", "pk": 69631, "fields": {"target": 1347, "key": "Previous CTOI", "value": "", "float_value": null, "bool_value": false, "time_value": null}}, {"model": "tom_targets.targetextra", "pk": 69632, "fields": {"target": 1347, "key": "Master", "value": "5", "float_value": 5.0, "bool_value": true, "time_value": "2021-03-05T00:00:00Z"}}, {"model": "tom_targets.targetextra", "pk": 69633, "fields": {"target": 1347, "key": "SG1A", "value": "5", "float_value": 5.0, "bool_value": true, "time_value": "2021-03-05T00:00:00Z"}}, {"model": "tom_targets.targetextra", "pk": 69634, "fields": {"target": 1347, "key": "SG1B", "value": "5", "float_value": 5.0, "bool_value": true, "time_value": "2021-03-05T00:00:00Z"}}, {"model": "tom_targets.targetextra", "pk": 69635, "fields": {"target": 1347, "key": "SG2", "value": "5", "float_value": 5.0, "bool_value": true, "time_value": "2021-03-05T00:00:00Z"}}, {"model": "tom_targets.targetextra", "pk": 69636, "fields": {"target": 1347, "key": "SG3", "value": "5", "float_value": 5.0, "bool_value": true, "time_value": "2021-03-05T00:00:00Z"}}, {"model": "tom_targets.targetextra", "pk": 69637, "fields": {"target": 1347, "key": "SG4", "value": "5", "float_value": 5.0, "bool_value": true, "time_value": "2021-03-05T00:00:00Z"}}, {"model": "tom_targets.targetextra", "pk": 69638, "fields": {"target": 1347, "key": "SG5", "value": "5", "float_value": 5.0, "bool_value": true, "time_value": "2021-03-05T00:00:00Z"}}, {"model": "tom_targets.targetextra", "pk": 69639, "fields": {"target": 1347, "key": "ACWG", "value": "", "float_value": null, "bool_value": false, "time_value": null}}, {"model": "tom_targets.targetextra", "pk": 69640, "fields": {"target": 1347, "key": "TESS Disposition", "value": "PC", "float_value": null, "bool_value": true, "time_value": null}}, {"model": "tom_targets.targetextra", "pk": 69641, "fields": {"target": 1347, "key": "TFOPWG Disposition", "value": "KP", "float_value": null, "bool_value": true, "time_value": null}}, {"model": "tom_targets.targetextra", "pk": 69642, "fields": {"target": 1347, "key": "Mag (TESS)", "value": "12.212", "float_value": 12.212, "bool_value": true, "time_value": null}}, {"model": "tom_targets.targetextra", "pk": 69643, "fields": {"target": 1347, "key": "Mag (TESS) err", "value": "0.007", "float_value": 0.007, "bool_value": true, "time_value": null}}, {"model": "tom_targets.targetextra", "pk": 69644, "fields": {"target": 1347, "key": "Planet Name", "value": "", "float_value": null, "bool_value": false, "time_value": null}}, {"model": "tom_targets.targetextra", "pk": 69645, "fields": {"target": 1347, "key": "Pipeline Signal ID", "value": "1", "float_value": 1.0, "bool_value": true, "time_value": "2021-03-01T00:00:00Z"}}, {"model": "tom_targets.targetextra", "pk": 69646, "fields": {"target": 1347, "key": "Source", "value": "spoc", "float_value": null, "bool_value": true, "time_value": null}}, {"model": "tom_targets.targetextra", "pk": 69647, "fields": {"target": 1347, "key": "RA", "value": "11:00:40.13", "float_value": null, "bool_value": true, "time_value": "2021-03-01T11:00:40.130Z"}}, {"model": "tom_targets.targetextra", "pk": 69648, "fields": {"target": 1347, "key": "Dec", "value": "64:57:49.84", "float_value": null, "bool_value": true, "time_value": null}}, {"model": "tom_targets.targetextra", "pk": 69649, "fields": {"target": 1347, "key": "PM RA (mas/yr)", "value": "-22.389", "float_value": -22.389, "bool_value": true, "time_value": null}}, {"model": "tom_targets.targetextra", "pk": 69650, "fields": {"target": 1347, "key": "PM RA err (mas/yr)", "value": "0.044", "float_value": 0.044, "bool_value": true, "time_value": null}}, {"model": "tom_targets.targetextra", "pk": 69651, "fields": {"target": 1347, "key": "PM Dec (mas/yr)", "value": "-32.425", "float_value": -32.425, "bool_value": true, "time_value": null}}, {"model": "tom_targets.targetextra", "pk": 69652, "fields": {"target": 1347, "key": "PM Dec err (mas/yr)", "value": "0.046", "float_value": 0.046, "bool_value": true, "time_value": null}}, {"model": "tom_targets.targetextra", "pk": 69653, "fields": {"target": 1347, "key": "Epoch (BJD)", "value": "2458871.724298", "float_value": 2458871.724298, "bool_value": true, "time_value": null}}, {"model": "tom_targets.targetextra", "pk": 69654, "fields": {"target": 1347, "key": "Epoch (BJD) err", "value": "0.000575", "float_value": 0.000575, "bool_value": true, "time_value": null}}, {"model": "tom_targets.targetextra", "pk": 69655, "fields": {"target": 1347, "key": "Period (days)", "value": "1.706307", "float_value": 1.706307, "bool_value": true, "time_value": null}}, {"model": "tom_targets.targetextra", "pk": 69656, "fields": {"target": 1347, "key": "Period (days) err", "value": "6.3e-05", "float_value": 6.3e-05, "bool_value": true, "time_value": null}}, {"model": "tom_targets.targetextra", "pk": 69657, "fields": {"target": 1347, "key": "Duration (hours)", "value": "1.613599", "float_value": 1.613599, "bool_value": true, "time_value": null}}, {"model": "tom_targets.targetextra", "pk": 69658, "fields": {"target": 1347, "key": "Duration (hours) err", "value": "0.04555", "float_value": 0.04555, "bool_value": true, "time_value": null}}, {"model": "tom_targets.targetextra", "pk": 69659, "fields": {"target": 1347, "key": "Depth (mmag)", "value": "13.374045", "float_value": 13.374045, "bool_value": true, "time_value": "2021-03-13T00:00:00Z"}}, {"model": "tom_targets.targetextra", "pk": 69660, "fields": {"target": 1347, "key": "Depth (mmag) err", "value": "0.259323", "float_value": 0.259323, "bool_value": true, "time_value": null}}, {"model": "tom_targets.targetextra", "pk": 69661, "fields": {"target": 1347, "key": "Depth (ppm)", "value": "12242.395417", "float_value": 12242.395417, "bool_value": true, "time_value": null}}, {"model": "tom_targets.targetextra", "pk": 69662, "fields": {"target": 1347, "key": "Depth (ppm) err", "value": "238.81714", "float_value": 238.81714, "bool_value": true, "time_value": "0238-03-01T00:00:00Z"}}, {"model": "tom_targets.targetextra", "pk": 69663, "fields": {"target": 1347, "key": "Planet Radius (R_Earth)", "value": "10.611799", "float_value": 10.611799, "bool_value": true, "time_value": "2021-03-10T00:00:00Z"}}, {"model": "tom_targets.targetextra", "pk": 69664, "fields": {"target": 1347, "key": "Planet Radius (R_Earth) err", "value": "0.748163", "float_value": 0.748163, "bool_value": true, "time_value": null}}, {"model": "tom_targets.targetextra", "pk": 69665, "fields": {"target": 1347, "key": "Planet Insolation (Earth Flux)", "value": "652.774216", "float_value": 652.774216, "bool_value": true, "time_value": "0652-03-01T00:00:00Z"}}, {"model": "tom_targets.targetextra", "pk": 69666, "fields": {"target": 1347, "key": "Planet Equil Temp (K)", "value": "1289.170911", "float_value": 1289.170911, "bool_value": true, "time_value": "1289-03-01T00:00:00Z"}}, {"model": "tom_targets.targetextra", "pk": 69667, "fields": {"target": 1347, "key": "Planet SNR", "value": "34.111683", "float_value": 34.111683, "bool_value": true, "time_value": "2034-03-01T00:00:00Z"}}, {"model": "tom_targets.targetextra", "pk": 69668, "fields": {"target": 1347, "key": "Stellar Distance (pc)", "value": "262.706", "float_value": 262.706, "bool_value": true, "time_value": "0262-03-01T00:00:00Z"}}, {"model": "tom_targets.targetextra", "pk": 69669, "fields": {"target": 1347, "key": "Stellar Distance (pc) err", "value": "2.595", "float_value": 2.595, "bool_value": true, "time_value": "2021-03-02T00:00:00Z"}}, {"model": "tom_targets.targetextra", "pk": 69670, "fields": {"target": 1347, "key": "Stellar Eff Temp (K)", "value": "4974.0", "float_value": 4974.0, "bool_value": true, "time_value": null}}, {"model": "tom_targets.targetextra", "pk": 69671, "fields": {"target": 1347, "key": "Stellar Eff Temp (K) err", "value": "130.639", "float_value": 130.639, "bool_value": true, "time_value": "0130-03-01T00:00:00Z"}}, {"model": "tom_targets.targetextra", "pk": 69672, "fields": {"target": 1347, "key": "Stellar log(g) (cm/s^2)", "value": "4.44059", "float_value": 4.44059, "bool_value": true, "time_value": "2021-03-04T00:00:00Z"}}, {"model": "tom_targets.targetextra", "pk": 69673, "fields": {"target": 1347, "key": "Stellar log(g) (cm/s^2) err", "value": "0.093149", "float_value": 0.093149, "bool_value": true, "time_value": null}}, {"model": "tom_targets.targetextra", "pk": 69674, "fields": {"target": 1347, "key": "Stellar Radius (R_Sun)", "value": "0.902915", "float_value": 0.902915, "bool_value": true, "time_value": null}}, {"model": "tom_targets.targetextra", "pk": 69675, "fields": {"target": 1347, "key": "Stellar Radius (R_Sun) err", "value": "0.059864", "float_value": 0.059864, "bool_value": true, "time_value": null}}, {"model": "tom_targets.targetextra", "pk": 69676, "fields": {"target": 1347, "key": "Stellar Metallicity", "value": "", "float_value": null, "bool_value": false, "time_value": null}}, {"model": "tom_targets.targetextra", "pk": 69677, "fields": {"target": 1347, "key": " Stellar Metallicity err", "value": "", "float_value": null, "bool_value": false, "time_value": null}}, {"model": "tom_targets.targetextra", "pk": 69678, "fields": {"target": 1347, "key": "Sectors", "value": "21", "float_value": 21.0, "bool_value": true, "time_value": "2021-03-21T00:00:00Z"}}, {"model": "tom_targets.targetextra", "pk": 69679, "fields": {"target": 1347, "key": "Date TOI Alerted (UTC)", "value": "2020-03-12 15:35:30", "float_value": null, "bool_value": true, "time_value": "2020-03-12T15:35:30Z"}}, {"model": "tom_targets.targetextra", "pk": 69680, "fields": {"target": 1347, "key": "Date TOI Updated (UTC)", "value": "2020-03-12 15:35:30", "float_value": null, "bool_value": true, "time_value": "2020-03-12T15:35:30Z"}}, {"model": "tom_targets.targetextra", "pk": 69681, "fields": {"target": 1347, "key": "Date Modified", "value": "2020-03-13 12:00:26", "float_value": null, "bool_value": true, "time_value": "2020-03-13T12:00:26Z"}}]'
        for deserialized_obj in deserialize("json", target1_extras_serialized):
            deserialized_obj.save()

        transit_serialized = '[{"model": "exotom.transit", "pk": 5274, "fields": {"target": 1347, "number": 270, "start": "2021-04-27T21:25:10.571Z", "mid": "2021-04-27T22:13:35.049Z", "end": "2021-04-27T23:01:59.527Z"}}]'
        deserialized_obj = deserialize("json", transit_serialized).__next__()
        deserialized_obj.save()
        self.transit1 = deserialized_obj.object

        obs_record_ser = '[{"model": "tom_observations.observationrecord", "pk": 1104, "fields": {"target": 1347, "user": null, "facility": "IAGTransit", "parameters": {"ipp_value": 1.05, "exposure_time": 13.749552287834481, "max_airmass": 2.0, "facility": "IAGTransit", "target_id": 1347, "observation_type": "", "name": "TOI 1780.01 #270", "min_lunar_distance": null, "transit_id": 5154, "filter": "Clear", "instrument_type": "0M5 SBIG6303E", "readout_mode": "sbig6303e_2x2", "transit": "270"}, "observation_id": "14495", "status": "COMPLETED", "scheduled_start": "2021-04-27T20:21:54.312Z", "scheduled_end": "2021-04-27T23:56:54.312Z", "created": "2021-04-27T15:25:55.886Z", "modified": "2021-07-26T08:56:46.467Z"}}]'
        deserialized_obj = deserialize("json", obs_record_ser).__next__()
        deserialized_obj.save()
        obs_record = deserialized_obj.object

        self.all_lightcurve_dp = DataProduct()
        self.all_lightcurve_dp.data_product_type = "transit_all_light_curves"
        self.all_lightcurve_dp.observation_record = obs_record
        self.all_lightcurve_dp.target = self.target1
        with open(self.data_file1) as f:
            self.all_lightcurve_dp.data.save(
                "test_light_curve_1780_#270_all.csv", File(f)
            )
        # self.all_lightcurve_dp.extra_data = '{"light_curve_name": "test_light_curve_1780_#270", "light_curve_optimization_method": "test light curve opt method"}'
        self.all_lightcurve_dp.save()

        self.fluxrad50_dp = DataProduct()
        self.fluxrad50_dp.data_product_type = "fluxrad50"
        self.fluxrad50_dp.observation_record = obs_record
        self.fluxrad50_dp.target = self.target1
        with open(self.fluxrad50_path) as f:
            self.fluxrad50_dp.data.save(
                "test_light_curve_1780_#270_fluxrad50.csv", File(f)
            )
        self.fluxrad50_dp.save()

    # comment out to analyze data products
    def tearDown(self) -> None:
        # print(MEDIA_ROOT)
        print("Deleting all data product files")
        for dp in DataProduct.objects.all():
            print(f"Deleting {dp.data.path}")
            path = dp.data.path
            os.remove(path)

            # try to delete two folder up from this file, if theyre empty
            up_one_dir = os.path.normpath(f"{path}/..")
            if len(os.listdir(up_one_dir)) == 0:
                os.rmdir(up_one_dir)
                up_two_dir = os.path.normpath(f"{up_one_dir}/..")
                if len(os.listdir(up_two_dir)) == 0:
                    os.rmdir(up_two_dir)

    def test_find_best_lightcurve(self):

        for method in [
            "brightest_ref_star",
            "all_ref_stars",
            "outlier_removal_all_ref_stars",
            "outlier_removal_normaltest_filter_all_ref_stars",
            "full",
        ]:
            with self.subTest(i=method):
                print(f"\n\nTesting light curve optimization method {method}")
                lc = AllLightCurves(self.all_lightcurve_dp, self.fluxrad50_dp)
                lc.create_best_lightcurve(method=method)

                best_dp = lc.best_light_curves_dp
                self.assertIsNotNone(best_dp)
                self.assertTrue(os.path.exists(best_dp.data.path))

                self.assertIsNotNone(lc.best_loss_sliding_window_std)
                self.assertIsNotNone(lc.best_loss_fwhm_correlation_p)
